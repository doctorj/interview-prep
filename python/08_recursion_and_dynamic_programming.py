"""
Solutions to "Cracking The Coding Interview" problems on Recursion and Dynamic Programming.
"""
import unittest
import random
from collections import Counter
from itertools import product, tee, chain, permutations
from functools import lru_cache as memo
from typing import Tuple, Optional, List, Iterable, TypeVar, Sequence, Set, Union, Counter as CounterType, FrozenSet, Iterator


T = TypeVar('T')


@memo()
def triple_step(n: int) -> int:
    """:Return: the number of possible ways to run up `n` stairs by taking either 1, 2, or 3 steps
    at a time.  (8.1)

    Recurse on the number of ways to run up n-1 stairs plus a single step, n-2 plus a double step, and
    n-3 plus a triple step.
    """
    if n < 0:
        return 0
    elif n == 0:
        return 1
    else:
        return triple_step(n - 1) + triple_step(n - 2) + triple_step(n - 3)


@memo()
def robot_in_a_grid(maze: Tuple[Tuple[bool, ...], ...], r: int = 0, c: int = 0) -> Optional[List[Tuple[int, int]]]:
    """Find a path from the top left ([0][0]) of `maze` to the bottom right by moving only right and down.  (8.2)

    Recurse finding a path to adjacent squares, returning this square + path.

    :param maze: ``maze[r][c] == False` means you cannot move to that spot.
    :returns: A path from (r, c) to the bottom right, or None if no path exists.
    """
    ROWS, COLS = len(maze), len(maze[0])
    if r >= ROWS or c >= COLS:
        return None
    if r == ROWS - 1 and c == COLS - 1:
        return [(r, c)]
    if not maze[r][c]:
        return None
    adjacent = ((r + 1, c), (r, c + 1))
    path = min(filter(None, (robot_in_a_grid(maze, rr, cc) for rr, cc in adjacent)), key=len, default=None)     # type: ignore      # https://github.com/python/mypy/issues/3354
    if path:
        return [(r, c)] + path
    return None


def magic_index(a: Sequence[int], lo: Optional[int] = None, hi: Optional[int] = None) -> Optional[int]:
    """:Return: an index i for which ``a[i] == i``, or None if not present.  (8.3)

    Recursive binary search of left half if mid is too high (right half will be too high also since sorted),
    otherwise right half.

    :param a: Sorted sequence of distinct integers.
    """
    lo, hi = 0 if lo is None else lo, len(a) - 1 if hi is None else hi
    if lo > hi:
        return None
    mid = (lo + hi) // 2
    if a[mid] == mid:
        return mid
    elif a[mid] > mid:
        return magic_index(a, lo, mid - 1)
    else:
        return magic_index(a, mid + 1, hi)


def power_set(s: Iterable[T]) -> Tuple[Set[T], ...]:
    """:Return: the power set (set of all subsets) of an iterable of (hashable) items `s`.  (8.4)

    The power set of S union {a} (for any a not in S) is the power set of S, plus the power set
    of S with a added to each subset.
    """
    if not isinstance(s, set):
        s = set(s)
    if not s:
        return (s,)
    elt = s.pop()
    ps = power_set(s)
    return tuple(chain(ps, (ss.union({elt}) for ss in ps)))


def recursive_multiply(a: int, b: int) -> int:
    """:Return: the product of positive integers a and b using only addition, subtraction, shifting, and recursion. (8.5)

    For each nonzero bit i of b, sum a << i.
    """
    if b == 0:
        return 0
    return (a if b & 1 else 0) + recursive_multiply(a << 1, b >> 1)


def towers_of_hanoi(t1: list, t2: list, t3: list, n: int) -> None:
    """Move top `n` disks from tower / stack / list `t1` to tower / stack /list `t3` using `t2` as temp space.  (8.6)

    Recursively move n-1 disks from t1 -> t2, move remaining disk from t1 -> t3, recursively move everything from t2 -> t3.
    """
    if n > 0:
        towers_of_hanoi(t1, t3, t2, n - 1)      # Move top n-1 items from t1 -> t2
        t3.append(t1.pop())                     # Move next on t1 to t3
        towers_of_hanoi(t2, t1, t3, n - 1)      # Move top n-1 items from t2 -> t3


def permutations_without_dupes(s: Sequence[T]) -> Iterable[Tuple[T, ...]]:
    """:Return: all permutations of sequence of unique items `s`.  (8.7)

    Recursively generate all permutations without the first item, then put the first item back
    in all possible positions of each permutation.
    """
    #assert len(s) == len(set(s))
    s = tuple(s)
    if len(s) <= 1:
        return iter((s,))
    else:
        first, rest = s[0:1], s[1:]
        return (p[:k] + first + p[k:] for p in permutations_without_dupes(rest) for k in range(len(rest) + 1))


def permutations_with_dupes(s: Union[Sequence[T], CounterType[T]]) -> Iterable[Tuple[T, ...]]:
    """:Return: all permutations of a sequence of (possibly repeated) items `s`.  (8.8)

    For each unique item, recursively generate all permutations with one less of that item, then prepend
    that item to the resulting permutations.
    """
    ctr = Counter(s) if not isinstance(s, Counter) else s
    if not ctr:
        return iter(((),))      # Would be more efficient to loop: decrement counter, recurse, increment counter
    return ((item,) + perm for item in ctr for perm in permutations_with_dupes(ctr - Counter({item: 1})))


def parens(n: int) -> FrozenSet[str]:
    """:Return: the set of all valid matched combinations of `n` pairs of parentheses.  (8.9)

    Recursively put parens left, around, and right of each combination of n-1 parens.
    """
    if n == 0:
        return frozenset()
    if n == 1:
        return frozenset(('()',))
    return frozenset(chain.from_iterable(('()' + p, '(' + p + ')', p + '()') for p in parens(n - 1)))


def paint_fill(screen: List[List[int]], r: int, c: int, color: int) -> None:
    """Flood-fill a `screen` of pixels, such that all pixels the same color as and connected to pixel `(r, c)`
    are set to the new `color`.  (8.10)

    Recurisvely fill adjacent, in-bounds, same-color neighbors.
    """
    def in_bounds(row: int, col: int) -> bool:
        return 0 <= row < len(screen) and 0 <= col < len(screen[0])

    if not in_bounds(r, c) or screen[r][c] == color:
        return
    old_color = screen[r][c]
    screen[r][c] = color
    for rr, cc in ((r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)):
        if in_bounds(rr, cc) and screen[rr][cc] == old_color:
            paint_fill(screen, rr, cc, color)


@memo()
def coins(c: int, denoms: Tuple[int, ...] = (25, 10, 5, 1)) -> int:
    """:Return: the number of ways of representing `c` cents with coints of the given denominations.  (8.11)

    For all valid quantities of the first coin (denomination), recursively make change for the remainder without
    that coin, and sum the results.
    """
    if len(denoms) <= 1:
        return 1
    return sum(coins(c - denoms[0] * k, denoms[1:]) for k in range(c // denoms[0] + 1))


def _valid_queen_pos(columns: List[Optional[int]], row: int, col: int) -> bool:
    """:Return: true iff `(row, col)` is a valid placement of a queen in 'map' of row number to column number `columns`."""
    # Not same column, and not same diag.  Same diag when row dist == col dist.
    return all(col != cc and abs(col - cc) != abs(row - rr) for rr, cc in enumerate(columns) if cc is not None)


def _place_queens(columns: List[Optional[int]], row: int) -> Iterator[Tuple[int, ...]]:
    """Given a list of queens `columns` for each row of a chessboard, return all valid positions for
    ``len(columns)`` queens on a square chess board of size ``len(columns)``, such that none share the same row, column, or diagonal.

    :param columns: columns[r] gives the column of the queen on row r (since no two can share a row).
    :param row: Place queens on all rows greater than or equal to this one.
    """
    if row == len(columns):
        assert None not in columns
        yield tuple(c for c in columns if c is not None)        # For typechecker
    else:
        for col in range(len(columns)):
            if _valid_queen_pos(columns, row, col):
                columns[row] = col
                for pos in _place_queens(columns, row + 1):
                    yield pos
                columns[row] = None


def eight_queens(n: int) -> Iterator[Tuple[int, ...]]:
    """:Return: all positions for `n` queens on an nxn chess board, such that none share the same row, column,
    or diagonal.  (8.12)

    For each (unplaced) row, recursively check each column for validity; when you hit the board size, yield a solution.
    """
    return _place_queens([None] * n, 0)


def pairwise(iterable: Iterable[T]) -> Iterable[Tuple[T, T]]:
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


class TestChapter8(unittest.TestCase):
    def test_triple_step(self) -> None:
        vecs = ((0, 1), (1, 1), (2, 2), (3, 4), (4, 7), (5, 13),)
        for steps, ways in vecs:
            self.assertEqual(triple_step(steps), ways)

    @staticmethod
    def random_sorted_array(n: int, mindiff: int = 1, maxdiff: int = 10) -> List[int]:
        """:Return: a sorted array of length `n` with (first) differences at least `mindiff` and at most `maxdiff`."""
        assert 0 <= mindiff <= maxdiff
        a = [random.randint(mindiff, maxdiff) for _ in range(n)]
        for i in range(1, len(a)):
            a[i] += a[i - 1]
        return a

    @staticmethod
    def is_valid_path(maze: Tuple[Tuple[bool, ...], ...], path: Optional[List[Tuple[int, int]]]) -> bool:
        if path:
            return (path[0] == (0, 0) and
                    all(maze[r][c] for r, c in path) and
                    all((r2, c2) == (r1 + 1, c1) or (r2, c2) == (r1, c1 + 1) for (r1, c1), (r2, c2) in pairwise(path)) and
                    path[-1] == (len(maze) - 1, len(maze[0]) - 1))
        else:
            return True     # Hope there's no path!

    def test_robot_in_a_grid(self) -> None:
        X, _ = False, True
        mazes = (
            ((_, _),
             (_, _)),

            ((_, _, X),
             (X, _, X),
             (X, _, _)),

            ((_, X),
             (X, _)),

            ((_, _, X, X,),
             (X, _, _, X,),
             (X, X, _, _,),
             (X, X, X, _,),),

            ((_, _, X, X,),
             (X, _, _, _,),
             (X, X, X, _,),
             (X, X, _, _,),
             (X, X, _, X,),
             (X, X, _, _,),),
        )       # type: Tuple[Tuple[Tuple[bool, ...] ,...] ,...]
        for maze in mazes:
            path = robot_in_a_grid(maze)
            self.assertTrue(self.is_valid_path(maze, path), '{}\n{}'.format(maze, path))

    def test_magic_index(self) -> None:
        vecs = (
            #0   1   2   3   4   5
            (0,),
            (1,  2,  3),
            (-1, 0,  1),
            (-1, 0,  2),
            (-1, 0,  2,  4,  6,  8),
            (-2, -1, 0,  3,  4,  6),
        )       # type: Tuple[Tuple[int, ...], ...]
        for a in vecs:
            # Verbose to appease typechecker
            magic = frozenset(i for i in range(len(a)) if a[i] == i)
            result = magic_index(a)
            if result is None:
                self.assertEqual(len(magic), 0)
            else:
                self.assertIn(result, magic)

    def test_power_set(self) -> None:
        vecs = (
            ((), ((),)),
            ((1,), ((), (1,))),
            ((1, 2), ((), (1,), (2,), (1, 2))),
            ((1, 2, 3), ((), (1,), (2,), (3,), (1, 2), (1, 3), (2, 3), (1, 2, 3))),
        )       # type: Tuple[Tuple[Tuple[int, ...], Tuple[Tuple[int, ...], ...]], ...]
        for s, ps in vecs:
            expected = tuple(set(ss) for ss in ps)
            self.assertCountEqual(power_set(s), expected)

    def test_recursive_multiply(self) -> None:
        for a, b in product(range(600), repeat=2):
            self.assertEqual(recursive_multiply(a, b), a * b)
        for _ in range(10000):
            a, b = random.randint(0, 2 ** 32), random.randint(0, 2 ** 32)
            self.assertEqual(recursive_multiply(a, b), a * b)

    def test_towers_of_hanoi(self) -> None:
        for n in range(22):
            t1, t2, t3 = list(reversed(range(n))), [], []       # type: List[int], List[int], List[int]
            towers_of_hanoi(t1, t2, t3, n)
            self.assertListEqual(t1, [])
            self.assertListEqual(t2, [])
            self.assertListEqual(t3, list(reversed(range(n))))

    def test_permutations_without_dupes(self) -> None:
        for n in range(10):
            s = tuple(range(n))
            self.assertCountEqual(permutations(s), permutations_without_dupes(s))

    def test_permutations_with_dupes(self) -> None:
        for size in range(7):
            for _ in range(20):
                s = []      # type: List[int]
                for __ in range(size + 1):
                    s.append((s[-1] if s else 0) + random.randint(0, 1))
                self.assertCountEqual(frozenset(permutations(s)), permutations_with_dupes(s))

    def test_parens(self) -> None:
        vecs = (
            (),
            ('()',),
            ('()()', '(())'),
            ('()()()', '(()())', '()(())', '((()))', '(())()'),
        )       # type: Tuple[Tuple[str, ...], ...]
        for n, exp in enumerate(vecs):
            self.assertSetEqual(frozenset(exp), parens(n))

    def test_paint_fill(self) -> None:
        vecs = (
            ([[0]],
             [[1]],
             0, 0),
            ([[0, 0],
              [0, 0]],
             [[1, 1],
              [1, 1]],
             0, 0),
            ([[1, 1],
              [1, 1]],
             [[1, 1],
              [1, 1]],
             0, 0),
            ([[0, 0],
              [0, 2]],
             [[1, 1],
              [1, 2]],
             0, 0),
            ([[0, 0, 2],
              [2, 0, 0],
              [2, 2, 0]],
             [[1, 1, 2],
              [2, 1, 1],
              [2, 2, 1]],
             0, 0),
            ([[3, 3, 2],
              [2, 0, 3],
              [2, 2, 0]],
             [[3, 3, 2],
              [2, 1, 3],
              [2, 2, 0]],
             1, 1),
        )
        for screen, output, r, c in vecs:
            paint_fill(screen, r, c, 1)
            self.assertListEqual(output, screen)

    def test_coins(self) -> None:
        vecs = ((1, 1), (2, 1), (3, 1), (4, 1), (5, 2), (6, 2), (7, 2), (8, 2), (9, 2), (10, 4), (11, 4), (15, 6), (20, 9), (25, 13), (100, 242))
        for cents, ways in vecs:
            self.assertEqual(coins(cents), ways)

    def test_eight_queens(self) -> None:
        vecs = (
            ((),),
            ((0,),),
            (),     # There are no solutions for n = 2 or 3
            (),
            ((1, 3, 0, 2), (2, 0, 3, 1)),
            ((0, 2, 4, 1, 3), (0, 3, 1, 4, 2), (1, 3, 0, 2, 4), (1, 4, 2, 0, 3), (2, 0, 3, 1, 4), (2, 4, 1, 3, 0), (3, 0, 2, 4, 1), (3, 1, 4, 2, 0), (4, 1, 3, 0, 2), (4, 2, 0, 3, 1)),
        )       # type: Tuple[Tuple[Tuple[int, ...], ...], ...]
        for n, positions in enumerate(vecs):
            self.assertCountEqual(positions, eight_queens(n))
