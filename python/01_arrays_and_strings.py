"""
Solutions to "Cracking The Coding Interview" problems on Arrays and Strings.
"""
import unittest
from collections import Counter
from itertools import permutations


def is_unique(s):
    """:Return: True if the string `s` contains all unique characters. (1.1)"""
    return max(Counter(s).values(), default=1) == 1        # Python makes it too easy


def check_permutation(s1, s2):
    """:Return: True iff strings `s1` and `s2` are permutations of each other. (1.2)"""
    return Counter(s1) == Counter(s2)


def urlify(s):
    """:Return: `s` with spaces replaced by '%20' (1.3)"""
    return s.replace(' ', '%20')        # Thanks, Python


def palindrome_permutation(s):
    """:Return: True iff string `s` is a permutation of a palindrome. (1.4)"""
    return sum(1 for count in Counter(s).values() if count % 2 == 1) <= 1       # At most one character has odd parity


def one_away(s1, s2):
    """:Return: True iff strings `s1` and `s2` are at most one edit away. (1.5)"""
    a, b = sorted((s1, s2), key=len)        # a is shorter
    return (
        s1 == s2 or
        (len(a) == len(b) and any(a[:pos] + b[pos] + a[pos + 1:] == b for pos in range(len(a)))) or      # Replacements (can only work when same length)
        (len(b) == len(a) + 1 and any(a[:pos] + b[pos] + a[pos:] == b for pos in range(len(b))))         # inserts and removes (can only work if shorter is 1 less than longer; insert in shorter is same as remove from longer)
    )


class TestChapter1(unittest.TestCase):
    def test_is_unique(self):
        unique = ('', 'a', 'ab', 'abc', 'zxy', 'abcdefghijklmnopqrstuvwxyz', 'thequickbrownfx', '123XYZ')
        non = ('aa', 'aba', 'aab', 'abcdefghijklmnopqrstuvwxyza', 'racecar')
        for s in unique:
            self.assertTrue(is_unique(s), s)
        for s in non:
            self.assertFalse(is_unique(s), s)

    def test_check_permutation(self):
        perms = (('', ''), ('a', 'a'), ('ab', 'ab'), ('ab', 'ba'), ('abc', 'abc'), ('abc', 'acb'), ('abc', 'bac'), ('abc', 'bca'), ('abc', 'cab'), ('abc', 'cba'),
                 ('racecar', 'racecar'), ('aa', 'aa'), ('aab', 'aab'), ('aba', 'aab'), ('aabb', 'abab'), ('aabbc', 'abcab'))
        non = (('', 'a'), ('aa', 'a'), ('ab', 'aa'), ('aba', 'abb'), ('abcabc', 'abcabd'))
        for s1, s2 in perms:
            self.assertTrue(check_permutation(s1, s2))
        for s1, s2 in non:
            self.assertFalse(check_permutation(s1, s2))

    def test_urlify(self):
        vecs = (('', ''), ('a', 'a'), ('a ', 'a%20'), ('a b', 'a%20b'))
        for s, exp in vecs:
            self.assertEqual(urlify(s), exp)

    def test_palindrome_permutation(self):
        pals = ('', 'a', 'aa', 'redivider', 'noon', 'civic', 'radar', 'level', 'rotor', 'kayak', 'reviver', 'racecar', 'redder', 'madam', 'refer')
        non = ('ab', 'the', 'quick', 'brown', 'fox')
        for pal in pals:
            for perm in permutations(pal):
                self.assertTrue(palindrome_permutation(perm))
        for s in non:
            for perm in permutations(s):
                self.assertFalse(palindrome_permutation(perm))

    def test_one_away(self):
        one = (('', ''), ('a', 'a'), ('', 'a'), ('a', ''), ('a', 'ab'), ('ab', 'abc'), ('ab', 'a'), ('a', 'b'), ('ab', 'ac'),
               ('abcdef', 'abddef'), ('abcdef', 'abcdex'), ('abcd', 'abcdx'), ('abcd', 'abc'), ('aaa', 'aaaa'), ('aaa', 'aax'), ('aaa', 'aaax'))
        more = (('', 'xx'), ('', 'xxx'), ('x', 'xxx'), ('ab', 'cd'), ('ab', 'abcd'), ('ab', ''), ('abcd', 'ab'), ('abcd', 'xbcy'), ('abcabc', 'abdabd'))
        for s1, s2 in one:
            self.assertTrue(one_away(s1, s2), "'{}', '{}'".format(s1, s2))
        for s1, s2 in more:
            self.assertFalse(one_away(s1, s2), "'{}', '{}'".format(s1, s2))
