"""
Singly linked list.
"""
import random
import unittest
from itertools import product
from typing import Generic, TypeVar, Iterable, Iterator, Sequence, overload, Any, Optional, Callable

T = TypeVar('T')


class Node(Generic[T], object):         # pylint: disable=too-few-public-methods
    """Linked List node."""
    __slots__ = ('data', 'next')

    def __init__(self, data: T, next_: Optional['Node[T]'] = None) -> None:
        self.data = data
        self.next = next_


class LinkedList(Sequence[T]):
    """A singly-linked list."""
    def __init__(self, items: Iterable[T] = ()) -> None:
        self.head = Node[T](None, None)        # type: ignore    # Dummy head node simplifies things
        self.size = 0
        end = self.head
        for item in items:
            end.next = Node(item)
            end = end.next
            self.size += 1

    def appendleft(self, item: T) -> None:
        """Add `item` to the left end (head) of the list. O(1) time."""
        self.head.next = Node(item, self.head.next)
        self.size += 1

    def append(self, item: T) -> None:
        """Add `item` to the right (tail) end of the list.  O(N) time."""
        end = self.head
        while end.next is not None:
            end = end.next
        end.next = Node(item)
        self.size += 1

    def _getpred(self, matches: Callable[[T, int], bool]) -> Optional[Node[T]]:
        """:Return: the predecessor Node of the first item to satisfy a predicate, or None if no item does.

        :param matches: a callable ``func(data, index)`` taking a data item and its index and returning
          True iff the item is what you're looking for.
        """
        pred = self.head
        idx = 0
        while pred.next is not None:
            if matches(pred.next.data, idx):
                return pred
            pred = pred.next
            idx += 1
        return None

    @staticmethod
    def _index_is(index: int) -> Callable[[T, int], bool]:
        """:Return: A predicate that takes a list data item and its index and returns True if the index
        equals that given.

        No bounds checking.
        """
        def index_is(_: T, idx: int) -> bool:
            return idx == index
        return index_is

    def _normalize_index(self, index: int) -> int:
        """:Return: a positive version of possibly-negative `index`.

        :raises IndexError: If `index` is out of range."""
        if index < 0:
            index = len(self) + index
        if not 0 <= index < len(self):
            raise IndexError('Index {} out of range {}'.format(index, len(self)))
        return index

    def pop(self, index: int = -1) -> T:
        """Remove and return the item at the given `index`; returns last (rightmost) by default."""
        pred = self._getpred(self._index_is(self._normalize_index(index)))
        assert pred is not None and pred.next is not None        # for typechecker
        item = pred.next.data
        pred.next = pred.next.next
        self.size -= 1
        return item

    def insert(self, index: int, item: T) -> None:
        """Insert `item` at position `index`, which may be negative."""
        if index != len(self):      # Want to be able to insert at end; don't bounds-check in that case.
            pred = self._getpred(self._index_is(self._normalize_index(index)))
            assert pred is not None
            pred.next = Node(item, pred.next)
            self.size += 1
        else:
            self.append(item)

    @overload
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is left (head) and -1 is right (tail)."""
        if isinstance(index, int):
            return self._getpred(self._index_is(self._normalize_index(index))).next.data
        elif isinstance(index, slice):
            return list(self)[index]        # Ineffecient but easily captures Python's slicing semantics, which islice does not.
        else:
            raise TypeError('Invalid index type')

    def __iter__(self) -> Iterator[T]:
        """Iterate over items from left (head) to right (tail)."""
        node = self.head.next
        while node is not None:
            yield node.data
            node = node.next

    def __len__(self) -> int:
        return self.size

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False

    def __repr__(self) -> str:
        return 'LinkedList({})'.format(repr(tuple(self)))


class TestLinkedList(unittest.TestCase):
    iters = 50

    def test_init(self) -> None:
        self.assertListEqual(list(LinkedList()), [])
        self.assertListEqual(list(LinkedList((1,))), [1])
        self.assertListEqual(list(LinkedList(range(10))), list(range(10)))
        self.assertNotEqual(LinkedList(range(10)), list(range(9)))
        self.assertNotEqual(LinkedList(range(9)), list(range(10)))
        self.assertNotEqual(LinkedList(), list(range(1)))
        self.assertNotEqual(LinkedList(range(9)), object())

    def test_append(self) -> None:
        ll, lst = LinkedList[int](), list()
        for n in range(self.iters):
            ll.append(n)
            lst.append(n)
            self.assertListEqual(list(ll), lst)
            self.assertEqual(ll, lst)
            self.assertEqual(len(ll), len(lst))

        ll, lst = LinkedList[int](), list()
        for n in range(self.iters):
            item = random.randint(-self.iters, self.iters)
            ll.append(item)
            lst.append(item)
            self.assertListEqual(list(ll), lst)
            self.assertEqual(ll, lst)
            self.assertEqual(len(ll), len(lst))

    def test_appendleft(self) -> None:
        ll = LinkedList[int]()
        lst = list(ll)
        for n in range(self.iters):
            ll.appendleft(n)
            lst.insert(0, n)
            self.assertListEqual(list(ll), lst)
            self.assertEqual(ll, lst)
            self.assertEqual(len(ll), len(lst))

        ll, lst = LinkedList[int](), list()
        for n in range(self.iters):
            item = random.randint(-self.iters, self.iters)
            ll.appendleft(item)
            lst.insert(0, item)
            self.assertListEqual(list(ll), lst)
            self.assertEqual(ll, lst)
            self.assertEqual(len(ll), len(lst))

    def test_getitem(self) -> None:
        items = tuple(range(self.iters))
        ll = LinkedList[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(ll[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(list(ll[i1:i2]), list(items[i1:i2]))
        self.assertRaises(IndexError, ll.__getitem__, self.iters)

    def test_pop(self) -> None:
        lst = list(range(self.iters))
        ll = LinkedList[int](lst)
        while lst:
            item = lst.pop()
            self.assertEqual(ll.pop(), item)
            self.assertEqual(ll, lst)

        lst = list(range(self.iters))
        ll = LinkedList[int](lst)
        while lst:
            idx = random.randrange(len(lst))
            item = lst.pop(idx)
            self.assertEqual(ll.pop(idx), item)
            self.assertEqual(ll, lst)
            self.assertRaises(IndexError, ll.pop, len(lst))
            self.assertRaises(IndexError, ll.pop, -len(lst) - 1)

        self.assertRaises(IndexError, ll.pop)

    def test_insert(self) -> None:
        ll = LinkedList[int]()
        lst = list(ll)
        for _ in range(self.iters):
            index, item = random.randrange(len(lst) + 1), random.randrange(len(lst) + 1)
            lst.insert(index, item)
            ll.insert(index, item)
            self.assertEqual(ll, lst)

        self.assertRaises(IndexError, ll.insert, len(ll) + 1, None)

    def test_everything(self) -> None:
        for _ in range(self.iters):
            lst = [random.randrange(self.iters) for _ in range(random.randrange(self.iters))]
            ll = LinkedList[int](lst)
            for __ in range(self.iters):
                op = random.randint(0, 3)
                idx = random.randrange(len(lst) + 1)
                item = random.randrange(len(lst) + 1)
                if op == 0:     # insert
                    lst.insert(idx, item)
                    ll.insert(idx, item)
                elif op == 1:   # append
                    lst.append(item)
                    ll.append(item)
                elif op == 2:   # appendleft
                    lst.insert(0, item)
                    ll.appendleft(item)
                elif op == 3 and idx < len(lst):   # pop
                    lst.pop(idx)
                    ll.pop(idx)

                self.assertEqual(lst, ll)
                self.assertEqual(len(lst), len(ll))
                for idx, item in enumerate(lst):
                    self.assertEqual(ll[idx], item)
                    self.assertEqual(ll.index(item), lst.index(item))
