"""
Solutions to "Cracking The Coding Interview" problems on Linked Lists.
"""
import random
import unittest

from linkedlist import LinkedList


def remove_dupes(ll):
    """Remove duplicates from the unsorted linked list `ll`. (2.1)"""
    seen = set()
    node = ll.head
    while node.next is not None:
        if node.next.data in seen:
            node.next = node.next.next      # remove
            ll.size -= 1
        else:
            seen.add(node.next.data)
            node = node.next


def kth_to_last(ll, k):
    """:Return: the k-th to last element of a linked list, where k = 1 means the last element. (2.2)"""
    # Two pointers, one k ahead of the other; advance last to end, then first is k-th to end.
    assert k > 0
    first = last = ll.head.next
    for _ in range(k - 1):
        if last is None:
            raise ValueError('List does not have {} elements'.format(k))
        last = last.next

    while last.next is not None:
        last = last.next
        first = first.next

    return first.data


def delete_middle_node(ll, node):
    """Delete `node` (which is not the first or last) from a list given only a reference to that node. (2.3)

    (The entire list is passed so we can adjust its size).
    """
    # This overwrites the current node with the next one
    assert node is not None and node.next is not None
    node.data = node.next.data
    node.next = node.next.next
    ll.size -= 1


def partition(ll, value):
    """Partition a linked list so that all nodes < value come before all nodes >= value.  (2.4)

    `value` itself can be anywhere in the right partition.
    """
    # Keep pointers to the less-than part and the greater-than part
    # Prepend less-than nodes to less-than pointer; append greater-than nodes to greater-than pointer
    lt = gte = node = ll.head.next
    while node is not None:
        nxt = node.next
        if node.data < value:
            node.next = lt
            lt = node
        else:
            gte.next = node
            gte = node
        node = nxt
    gte.next = None
    ll.head.next = lt


def sum_lists(a, b):
    """:Return: a new linked list whose nodes are the digits of the sum of the numbers represented by the linked lists
    a and b. (2.5)

    a and b have their digits in reverse order (head is least-significant)."""
    # 1st grade addition 'algorithm'
    a = a.head.next
    b = b.head.next
    carry = 0
    s = LinkedList()
    while a is not None or b is not None:
        val = (a.data if a else 0) + (b.data if b else 0) + carry
        s.appendleft(val % 10)
        carry = val // 10
        a = a.next if a else None
        b = b.next if b else None
    if carry:
        s.appendleft(carry)
    return s


def is_palindrome(ll):
    """:Return: True if a linked list is a palindrome.  (2.6)"""
    a = tuple(ll)       # If you have to use extra space anyway, just do the easy thing.
    return all(a[i] == a[len(a) - 1 - i] for i in range(len(a) // 2))


def intersection(a, b):
    """:Return: the first shared node of linked lists `a` and `b`, if the lists intersect, or None if they don't.  (2.7)

    Shared is determined by reference, i.e., a and b contain the same Node object.
    """
    # Key observation is that once they intersect (share a Node), all remaining nodes must be the same, too.
    a, b = sorted((a, b), key=len)        # a is shorter of the two
    extra = len(b) - len(a)
    ap, bp = a.head.next, b.head.next
    # Intersection must start in last len(a) elts of b, otherwise it would be longer than a.  So skip first `extra` nodes.
    while bp and extra:
        bp = bp.next
        extra -= 1
    # Now we can pretend a and b have same length, and just iterate through both until the Nodes are the same.
    while ap:
        if ap is bp:
            return ap
        ap = ap.next
        bp = bp.next
    return None


def dedup(lst):
    """:Return: a new list with the same elements in the same order as `lst` but with duplicates removed."""
    seen = set()
    out = []
    for item in lst:
        if item not in seen:
            out.append(item)
            seen.add(item)
    return out


def digits(x):
    """:Return: a tuple of the base-10 digits of x, most-significant first."""
    return tuple(int(d) for d in str(x))


def from_digits(digis):
    """:Return: an integer from an iterable of base-10 digits, most-significant first."""
    return int(''.join(map(chr, digis)))


class TestChapter2(unittest.TestCase):
    iters = 50

    def test_remove_dupes(self):
        ll = LinkedList([0, 0, 0])
        remove_dupes(ll)
        self.assertSequenceEqual(ll, [0])
        for _ in range(self.iters):
            lst = [random.randrange(self.iters) for _ in range(self.iters)]
            ll = LinkedList(lst)
            remove_dupes(ll)
            self.assertListEqual(dedup(lst), list(ll))

    def test_kth_to_last(self):
        ll = LinkedList(range(self.iters))
        for k in range(1, self.iters + 1):
            self.assertEqual(kth_to_last(ll, k), len(ll) - k)

        for _ in range(self.iters):
            ll = LinkedList(random.randrange(self.iters) for __ in range(self.iters))
            for k in range(1, self.iters):
                self.assertEqual(kth_to_last(ll, k), ll[-k])

    def test_delete_middle_node(self):
        for k in range(1, self.iters - 1):
            lst = list(range(self.iters))
            ll = LinkedList(lst)
            node = ll._getpred(ll._index_is(k)).next    # Get ref to kth Node # pylint: disable=protected-access
            delete_middle_node(ll, node)
            lst.pop(k)
            self.assertSequenceEqual(ll, lst)

    def test_partition(self):
        for _ in range(self.iters):
            lst = [random.randrange(self.iters) for __ in range(self.iters)]
            for part in range(self.iters):
                ll = LinkedList(lst)
                partition(ll, part)
                self.assertCountEqual(lst, ll)
                lt = True
                for val in ll:
                    if val >= part:
                        lt = False
                    if not lt:
                        self.assertGreaterEqual(val, part)

    def test_sum_lists(self):
        for _ in range(self.iters):
            a, b = random.randrange(10 ** self.iters), random.randrange(10 ** self.iters)
            lla, llb = LinkedList(reversed(digits(a))), LinkedList(reversed(digits(b)))
            self.assertSequenceEqual(sum_lists(lla, llb), LinkedList(digits(a + b)))

    def test_is_palindrome(self):
        palindromes = ('', 'a', 'aa', 'aaa', 'redivider', 'noon', 'civic', 'radar', 'level', 'rotor', 'kayak', 'reviver', 'racecar', 'redder', 'madam', 'refer')      # Thanks, Wikipedia
        non = ('the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog')
        for word in palindromes:
            self.assertTrue(is_palindrome(LinkedList(word)))
        for word in non:
            self.assertFalse(is_palindrome(LinkedList(word)))

    def test_intersection(self):
        for _ in range(self.iters):
            a, b = LinkedList(range(random.randrange(1, self.iters))), LinkedList(range(random.randrange(1, self.iters)))
            self.assertIsNone(intersection(a, b))
            sharedlen = random.randint(1, min(len(a), len(b)))
            first = b._getpred(b._index_is(len(b) - sharedlen)).next        # pylint: disable=protected-access
            a._getpred(a._index_is(len(a) - sharedlen)).next = first       # Make last `sharedlen` nodes of a the last `sharedlen` nodes of b       # pylint: disable=protected-access
            self.assertIs(intersection(a, b), first)
