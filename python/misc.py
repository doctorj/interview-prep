"""Miscellaneous programming problems."""
import unittest
from typing import Dict, Tuple      # pylint: disable=unused-import


def price_books(books: Dict[int, int]) -> float:
    """There is a sale on five books at a bookstore.  One copy of any of the five books costs 8 EUR.
    If, however, you buy two different books from the series, you get a 5% discount on those two books.
    If you buy 3 different books, you get a 10% discount.
    With 4 different books, you get a 20% discount.
    If you go the whole hog, and buy all 5, you get a huge 25% discount.
    Note that if you buy, say, four books, of which 3 are different titles,
    you get a 10% discount on the 3 that form part of a set, but the fourth book still costs 8 EUR.

    http://web.archive.org/web/20160424022450/http://codingdojo.org/cgi-bin/index.pl?KataPotter"""
    prices = [0, 8, 8 * 2 * .95, 8 * 3 * .9, 8 * 4 * .8, 8 * 5 * .75]       # price[i] is price of i distinct books
    sets = [0] * len(prices)
    for size in range(len(books), 0, -1):
        sets[size], item = min((count, item) for item, count in books.items())
        books.pop(item)
        for book in books:
            books[book] -= sets[size]

    adjust = min(sets[5], sets[3])  # It's cheaper to replace sets of 5 and 3 with two sets of 4
    sets[5] -= adjust
    sets[3] -= adjust
    sets[4] += 2 * adjust

    return sum(price * num for price, num in zip(prices, sets))


class Test(unittest.TestCase):
    def test_price_books(self) -> None:
        vecs = (
            (0, dict()),
            (8, {0: 1}),
            (8, {1: 1}),
            (8, {2: 1}),
            (8, {3: 1}),
            (8, {4: 1}),
            (8 * 2, {0: 2}),
            (8 * 3, {1: 3}),
            (8 * 2 * 0.95, {0: 1, 1: 1}),
            (8 * 3 * 0.9, {0: 1, 2: 1, 4: 1}),
            (8 * 4 * 0.8, {0: 1, 1: 1, 2: 1, 4: 1}),
            (8 * 5 * 0.75, {0: 1, 1: 1, 2: 1, 3: 1, 4: 1}),
            (8 + (8 * 2 * 0.95), {0: 2, 1: 1}),
            (2 * (8 * 2 * 0.95), {0: 2, 1: 2}),
            ((8 * 4 * 0.8) + (8 * 2 * 0.95), {0: 2, 1: 1, 2: 2, 3: 1}),
            (8 + (8 * 5 * 0.75), {0: 1, 1: 2, 2: 1, 3: 1, 4: 1}),
            (2 * (8 * 4 * 0.8), {0: 2, 1: 2, 2: 2, 3: 1, 4: 1}),
            (3 * (8 * 5 * 0.75) + 2 * (8 * 4 * 0.8), {0: 5, 1: 5, 2: 4, 3: 5, 4: 4}),
        )       # type: Tuple[Tuple[float, Dict[int, int]], ...]
        for price, books in vecs:
            self.assertEqual(price, price_books(books))
