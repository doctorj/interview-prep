"""
Simple adjacency-list graph.
"""
import random
import unittest
from itertools import permutations, tee, chain, product

from typing import Generic, TypeVar, Iterable, Tuple, Optional, Dict, Set, FrozenSet, KeysView, cast, Callable  # https://github.com/PyCQA/pylint/issues/1063     # pylint: disable=unused-import

V = TypeVar('V')
E = Tuple[V, V]


class Graph(Generic[V]):
    """Simple adjacency-list graph."""
    def __init__(self, edges: Iterable[E[V]] = (), vertices: Iterable[V] = (), bidi: bool = False) -> None:
        """Create a new graph from an optional iterable of (uni-directional) edges.

        Vertices can be any hashable type except None.
        :param vertices: Any singleton vertices (those with no edges) can be specified here.  Duplicates are fine.
        :param bidi: If True, for any edge ``(u, v)`` added to the graph at any time, ensure ``(v, u)`` is also in the graph.
        """
        self.e = dict()     # type: Dict[V, Set[V]]     # map vertex to set of neighbor vertices
        self.bidi = bidi
        for v in vertices:
            self.add(v)
        for u, v in edges:
            self.add(u, v)

    @property
    def vertices(self) -> KeysView[V]:
        return self.e.keys()

    @property
    def edges(self) -> Iterable[E[V]]:
        return ((u, v) for u, s in self.e.items() for v in s)

    def neighbors(self, v: V) -> Iterable[V]:
        return iter(self.e[v])

    def add(self, v1: V, v2: Optional[V] = None) -> None:
        """Add a vertex or edge to the graph.  O(1)."""
        assert v1 is not None
        if v2 is None:
            self.e.setdefault(v1, set())
        else:
            self.e.setdefault(v1, set()).add(v2)
            self.e.setdefault(v2, set())
            if self.bidi:
                self.e[v2].add(v1)

    def _in_edge_list(self, v: V) -> bool:
        """:Return: True iff vertex `v` is in some edge list.  O(E)."""
        # Better way?
        try:
            next(filter(lambda vv: vv == v, chain.from_iterable(self.e.values())))
            return True
        except StopIteration:
            return False

    def remove(self, v1: V, v2: Optional[V] = None, cull: bool = True) -> None:
        """Remove a vertex or edge from the graph.

        Removing a vertex will remove all edges incident on it.  Removing a vertex is O(V).
        Removing an edge is O(1), except with cull = True and bidi = False (the default) when it is O(E).
        :param cull: If true, when removing an edge only, removing the last edge incident on a vertex will also remove that vertex.
        """
        assert v1 is not None
        if v2 is None:      # vertex removal
            for s in (self.e[ss] for ss in self.e[v1]) if self.bidi else self.e.values():
                s.discard(v1)       # Remove incoming edges
            self.e.pop(v1, None)    # Remove outgoing edges
        else:               # edge removal
            check_v1 = cull and ((not self.e[v1]) or self.e[v1] == {v2})       # If v1 has no outgoing edges, or exactly one to v2
            check_v2 = cull and ((not self.e[v2]) or (self.e[v2] == {v1} and self.bidi))       # If v2 has no outgoing edges, or exactly one to v1 that we're about to nuke
            self.e[v1].discard(v2)
            if self.bidi:
                self.e[v2].discard(v1)
            if check_v1 and (self.bidi or not self._in_edge_list(v1)):       # If v1 has no more outgoing edges, see if there are any incoming
                self.e.pop(v1)
            if check_v2 and (self.bidi or not self._in_edge_list(v2)):       # If v2 has no more outgoing edges, see if there are any incoming
                self.e.pop(v2, None)                            # v1 could == v2

    def is_path(self, v1: V, v2: V) -> bool:
        """:Return: True iff there is a directed path from `v1` to `v2`."""
        if v1 not in self.e or v2 not in self.e:
            raise ValueError('Unknown vertex.')
        if self.bidi:                   # Bidirectional search (split-maze)
            frontier1, seen1 = frozenset((v1,)), set()      # type: FrozenSet[V], Set[V]
            frontier2, seen2 = frozenset((v2,)), set()      # type: FrozenSet[V], Set[V]
            while frontier1 or frontier2:      # If we run out of neighbors to explore and we haven't found a path, we're done
                if not frontier1.isdisjoint(frontier2):
                    return True
                seen1.update(frontier1)
                frontier1 = frozenset(v for f in frontier1 for v in self.e[f] if v not in seen1)
                if not frontier2.isdisjoint(frontier1):
                    return True
                seen2.update(frontier2)
                frontier2 = frozenset(v for f in frontier2 for v in self.e[f] if v not in seen2)
            return False
        else:       # directed graph; just have to do BFS
            frontier, seen = frozenset((v1,)), set()        # type: FrozenSet[V], Set[V]
            while frontier:
                if v2 in frontier:
                    return True
                seen.update(frontier)
                frontier = frozenset(v for f in frontier for v in self.e[f] if v not in seen)
            return False

    def _dfs(self, visit: Callable[[V, int], bool], start: V, depth: int) -> bool:
        """Internal recursive helper for `dfs()`."""
        for neighbor in self.e[start]:
            if self._dfs(visit, neighbor, depth + 1):
                return True
        return visit(start, depth)

    def dfs(self, visit: Callable[[V, int], bool], start: Optional[V] = None) -> bool:
        """Depth first search.  Calls `visit(vertex, depth)` on each vertex until `visit` returns True.

        :return: True if a call to `visit()` returned True.
        :param start: If given, start the search at this node.
        """
        if not self.e:
            return False
        if start is None:
            start = next(iter(self.e))
        return self._dfs(visit, start, 0)

    def __repr__(self) -> str:
        return 'Graph({}, {}, bidi={})'.format(tuple(self.edges), tuple(v for v in self.vertices if not self.e[v]), self.bidi)

    def __str__(self) -> str:
        return '\n'.join('{}: {}'.format(v, sorted(s)) for v, s in sorted(self.e.items())) + '\n' + str(self.e)


def fully_connected(n: int) -> Iterable[Tuple[int, int]]:
    """:Return: an iterable of edges (pairs of ints) representing the fully connected graph on `n` vertices."""
    assert n >= 2
    return cast(Iterable[Tuple[int, int]], permutations(range(n), 2))


def line(n: int) -> Iterable[Tuple[int, int]]:
    """:Return: an interable of edges (pairs of ints) representing a directed line of n vertices (0 -> 1 -> 2 -> ... -> n - 1)."""
    assert n >= 2
    return _pairwise(range(n))


def tree(n: int, k: int = 2) -> Iterable[Tuple[int, int]]:
    """:Return: an iterable of edges (pairs of ints) representing the complete `k`-ary binary tree of `n` nodes, with edges from parent to child."""
    assert n >= 2
    return (((i - 1) // k, i) for i in range(1, n))


def random_connected(n: int, p: float = 0.5) -> Iterable[Tuple[int, int]]:
    """:Return: an iterable of edges (pairs of ints) representing the graph where each possible directed edge is present with probability `p`."""
    assert 0 <= p <= 1.0
    return filter(lambda e: random.random() <= p, fully_connected(n))


def get_vertices(edges: Iterable[E[V]]) -> FrozenSet[V]:
    """:Return: the set of all vertices present in a list of edges."""
    return frozenset(chain.from_iterable(edges))


def undirect(edges: Iterable[E[V]]) -> FrozenSet[E[V]]:
    """:Return: The union of all edges (u, v) and (v, u) for all (u, v) in `edges`."""
    return frozenset(chain.from_iterable(((u, v), (v, u)) for u, v in edges))


def direct(edges: Iterable[E[V]]) -> Iterable[E[V]]:
    """:Return: `edges` with only one of `(u, v)` or `(v, u)`."""
    seen = set()        # type: Set[E[V]]
    for u, v in edges:
        if (u, v) not in seen and (v, u) not in seen:
            seen.add((u, v))
            yield (u, v)


def _pairwise(iterable: Iterable[V]) -> Iterable[E[V]]:
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def _subtree(index: int, maxindex: int) -> Set[int]:
    """:Return: the set of indices of children of binary tree node `index`, up to `maxindex`."""
    if index >= maxindex:
        return set()
    children = {index}
    children.update(_subtree(2 * index + 1, maxindex))
    children.update(_subtree(2 * index + 2, maxindex))
    return children


class TestGraph(unittest.TestCase):
    num_vertices = 30      # There are some O(V^4) tests.
    data = (
        (tuple(fully_connected(num_vertices)), ()),
        (tuple(line(num_vertices)), ()),
        (tuple(tree(num_vertices)), ()),
        (tuple(random_connected(num_vertices)), ()),
        ((), (0,)),
        ((), (0, 1)),
        (((0, 1),), (0, 1)),
        (((0, 1), (1, 0)), (0, 1)),
        (((0, 1), (1, 0)), (0, 1, 2)),
        (((0, 1), (1, 2), (2, 3)), (4,)),
        (((0, 1), (1, 2), (2, 3)), (4, 6)),
    )       # type: Tuple[Tuple[Tuple[Tuple[int, int], ...], Tuple[int, ...]], ...]
    graphs = tuple((edges, get_vertices(edges).union(vertices)) for edges, vertices in data)     # type: Tuple[Tuple[Tuple[Tuple[int, int], ...], FrozenSet[int]], ...]

    def test_graph_generators(self) -> None:
        self.assertCountEqual(tuple(fully_connected(2)), ((0, 1), (1, 0)))
        self.assertCountEqual(tuple(fully_connected(3)), ((0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1)))
        self.assertCountEqual(tuple(line(2)), ((0, 1),))
        self.assertCountEqual(tuple(line(3)), ((0, 1), (1, 2)))
        self.assertCountEqual(tuple(line(4)), ((0, 1), (1, 2), (2, 3)))
        self.assertCountEqual(tuple(tree(2)), ((0, 1),))
        self.assertCountEqual(tuple(tree(3)), ((0, 1), (0, 2)))
        self.assertCountEqual(tuple(tree(4)), ((0, 1), (0, 2), (1, 3)))
        self.assertCountEqual(tuple(tree(5)), ((0, 1), (0, 2), (1, 3), (1, 4)))
        self.assertCountEqual(tuple(tree(6)), ((0, 1), (0, 2), (1, 3), (1, 4), (2, 5)))
        self.assertCountEqual(tuple(tree(7)), ((0, 1), (0, 2), (1, 3), (1, 4), (2, 5), (2, 6)))
        self.assertCountEqual(tuple(tree(7)), ((0, 1), (0, 2), (1, 3), (1, 4), (2, 5), (2, 6)))
        self.assertCountEqual(tuple(tree(8, 3)), ((0, 1), (0, 2), (0, 3), (1, 4), (1, 5), (1, 6), (2, 7)))

    def test_add(self) -> None:
        for edges, vertices in self.graphs:
            g = Graph(edges, vertices)
            self.assertCountEqual(g.edges, edges)
            self.assertSetEqual(frozenset(g.vertices), vertices)

            g = Graph(edges, vertices, bidi=True)
            self.assertCountEqual(g.edges, undirect(edges))
            self.assertSetEqual(frozenset(g.vertices), vertices)

    def test_remove(self) -> None:
        for edges, vertices in self.graphs:
            # directed, no cull
            g = Graph(edges, vertices, bidi=False)
            remain = list(edges)
            random.shuffle(remain)
            while remain:
                g.remove(*remain.pop(), cull=False)
                self.assertCountEqual(g.edges, remain)
                self.assertSetEqual(frozenset(g.vertices), vertices)        # All vertices should remain with cull = False

            # directed, cull
            g = Graph(edges, vertices, bidi=False)
            remain = list(edges)
            random.shuffle(remain)
            while remain:
                g.remove(*(remain.pop()), cull=True)
                self.assertCountEqual(g.edges, remain)
                self.assertSetEqual(frozenset(g.vertices), get_vertices(remain) | (vertices - get_vertices(edges)))     # Only vertices that were initially sigletons should remain with cull = True

            # undirected, no cull
            g = Graph(edges, vertices, bidi=True)
            remains = set(undirect(edges))
            while remains:
                u, v = remains.pop()
                remains.discard((v, u))
                g.remove(u, v, cull=False)
                self.assertCountEqual(g.edges, remains)
                self.assertSetEqual(frozenset(g.vertices), vertices)        # All vertices should remain with cull = False

            # undirected, cull
            g = Graph(edges, vertices, bidi=True)
            remains = set(undirect(edges))
            while remains:
                u, v = remains.pop()
                remains.discard((v, u))
                g.remove(u, v, cull=True)
                self.assertCountEqual(g.edges, remains)
                self.assertSetEqual(frozenset(g.vertices), get_vertices(remains) | (vertices - get_vertices(edges)))        # Only vertices that were initially sigletons should remain with cull = True

    def test_is_path(self) -> None:
        points = Graph(vertices=(1, 2))
        self.assertTrue(points.is_path(1, 1))
        self.assertFalse(points.is_path(1, 2))
        self.assertFalse(points.is_path(2, 1))

        g1 = Graph(((0, 1), (1, 2), (3, 4), (4, 5)), bidi=True)
        self.assertTrue(g1.is_path(0, 0))
        self.assertTrue(g1.is_path(0, 2))
        self.assertTrue(g1.is_path(2, 0))
        self.assertFalse(g1.is_path(0, 3))
        self.assertFalse(g1.is_path(4, 1))

        full = Graph(fully_connected(self.num_vertices))
        for a, b in fully_connected(self.num_vertices):
            self.assertTrue(full.is_path(a, b))

        linear = Graph(line(self.num_vertices), bidi=False)
        for a, b in product(range(self.num_vertices), repeat=2):
            if a <= b:
                self.assertTrue(linear.is_path(a, b), "{}, {}\n{}".format(a, b, linear))
            else:
                self.assertFalse(linear.is_path(a, b), "{}, {}\n{}".format(a, b, linear))

        utree = Graph(tree(self.num_vertices), bidi=True)
        for a, b in product(range(self.num_vertices), repeat=2):
            self.assertTrue(utree.is_path(a, b))
        dtree = Graph(tree(self.num_vertices), bidi=False)
        for node in range(self.num_vertices):
            children = _subtree(node, self.num_vertices)
            for other in range(self.num_vertices):
                self.assertIs(dtree.is_path(node, other), other in children)

        # Two initially disconnected components
        c1, c2 = frozenset(fully_connected(self.num_vertices // 2)), frozenset(((u + self.num_vertices // 2, v + self.num_vertices // 2) for (u, v) in fully_connected(self.num_vertices // 2)))
        g2 = Graph(chain(c1, c2))
        for a, b in c1:
            self.assertTrue(g2.is_path(a, b))
        for a, b in c2:
            self.assertTrue(g2.is_path(a, b))
        for a, b in product(get_vertices(c1), get_vertices(c2)):
            self.assertFalse(g2.is_path(a, b))
        # Connect components
        g2.add(0, self.num_vertices - 1)
        g2.add(self.num_vertices - 1, 0)
        for a, b in product(g2.vertices, repeat=2):
            self.assertTrue(g2.is_path(a, b))

    def test_neighbors(self) -> None:
        edges = tuple(fully_connected(self.num_vertices))
        g = Graph[int](edges)
        for v in range(self.num_vertices):
            self.assertCountEqual(tuple(g.neighbors(v)), tuple(u for u in range(self.num_vertices) if u != v))

        edges = tuple(line(self.num_vertices))
        g = Graph[int](edges)
        for v in range(self.num_vertices - 1):
            self.assertCountEqual(tuple(g.neighbors(v)), (v + 1,))
