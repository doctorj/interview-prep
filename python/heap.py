"""
Baisc binary min-heap (priority queue).
"""
import heapq
import random
import unittest
from abc import ABCMeta, abstractmethod
from itertools import islice, repeat

from typing import TypeVar, Sized, Iterable, Container, Iterator, Any, Optional


# As of 2/1/17, mypy 0.470, typing 3.5.3, Python does not have a functioning notion of Comparable.
# This hack will silence the typechecker in library code, but every usage in user code will generate a typecheck error
# (e.g., "Type argument 1 of "Heap" has incompatible value "int") even if you Comparable.register(int).
# https://github.com/python/mypy/wiki/Unsupported-Python-Features
# https://github.com/python/mypy/issues/1459 probably among others
class Comparable(metaclass=ABCMeta):                # pylint: disable=too-few-public-methods
    """Abstract base class for objects supporting comparison."""
    @abstractmethod
    def __lt__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __gt__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __le__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __ge__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

T = TypeVar('T', bound=Comparable)


class Heap(Sized, Iterable[T], Container[T]):       # typing.Collection is not available before Python 3.6
    """A min-priority queue implemented as a binary heap."""
    # We store the min at index 0, so the parent of node k is (k-1)//2, and the children of k are 2*k+1 and 2*k+2
    def __init__(self, items: Iterable[T] = ()) -> None:
        self.data = list(items)
        self.size = len(self.data)
        for idx in range((self.size - 1) // 2, -1, -1):     # heapify
            self._siftdown(idx)

    def push(self, item: T) -> None:
        """Add `item` to the heap."""
        if self.size == len(self.data):
            self._resize()
        self.data[self.size] = item     # Put in last spot
        idx = self.size
        while idx > 0 and self.data[idx] < self.data[(idx - 1) // 2]:     # Swap with parent until parent less or equal
            self.data[idx], self.data[(idx - 1) // 2] = self.data[(idx - 1) // 2], self.data[idx]
            idx = (idx - 1) // 2
        self.size += 1

    def pop(self) -> T:
        """:Return: and remove the minimum item from the heap."""
        if not self.size:
            raise ValueError('Empty Heap')
        item = self.data[0]
        self.size -= 1
        self.data[0] = self.data[self.size]     # Put last item in top spot
        self._siftdown(0)
        return item

    def peek(self) -> T:
        """:Return: the minimum item from the heap without removing it."""
        if self.size:
            return self.data[0]
        else:
            raise ValueError('Empty Heap')

    def _smallest_child(self, index: int) -> Optional[int]:
        """:Return: the index of the child node of `index` with the smallest value, or None if the node has no children."""
        child = 2 * index + 1
        if child >= self.size:
            return None
        if child + 1 < self.size and self.data[child + 1] < self.data[child]:
            child += 1
        return child

    def _siftdown(self, index: int) -> None:
        """Repeatedly swap the item at `index` with its smallest child until the smallest child is not smaller than it.

        Alternately, assuming any children of `index` are valid sub-heaps, but `index` may not be, fix it.
        """
        child = self._smallest_child(index)         # None if no children
        while child and self.data[child] < self.data[(child - 1) // 2]:                  # Swap with smallest child until greater or equal
            self.data[child], self.data[(child - 1) // 2] = self.data[(child - 1) // 2], self.data[child]
            child = self._smallest_child(child)

    def _resize(self, size: Optional[int] = None) -> None:
        if not size:
            size = 2 * (len(self.data) + 1)
        assert size >= len(self.data), 'Cannot resize below current size'
        self.data.extend(repeat(None, size - len(self.data)))       # type: ignore

    def _satisfies_heap_property(self) -> bool:
        """:Return: True iff each element is >= its parent."""
        return all(self.data[idx] >= self.data[(idx - 1) // 2] for idx in range(1, self.size))

    def __len__(self) -> int:
        return self.size

    def __iter__(self) -> Iterator[T]:
        return islice(self.data, self.size)

    def __contains__(self, item: Any) -> bool:
        return any(item == d for d in iter(self))

    def __eq__(self, other: Any) -> bool:
        """:Return: True iff this heap contains the same items (ignoring order) as `other` (which must be iterable)."""
        try:
            return sorted(iter(self)) == sorted(other)      # Can't use set since items may not be hashable.  Could be a little faster, but not asymptotically.
        except TypeError:
            return False


class HeapTest(unittest.TestCase):
    def test_init(self) -> None:
        h = Heap[int]()     # type: ignore
        self.assertEqual(len(h), 0)
        self.assertEqual(h, ())
        self.assertRaises(ValueError, h.peek)

        h = Heap[int](range(10))     # type: ignore
        self.assertEqual(len(h), 10)
        self.assertEqual(h, list(range(10)))

        items = tuple(random.randint(0, 1000) for _ in range(1000))
        h = Heap[int](items)     # type: ignore
        self.assertEqual(len(h), len(items))
        self.assertCountEqual(tuple(h), items)
        self.assertTrue(all(i in h for i in items))

    def test_push(self) -> None:
        iters = 1000
        h = Heap[int]()     # type: ignore
        for item in range(iters):
            h.push(item)
            self.assertEqual(h.peek(), 0)
            self.assertEqual(len(h), item + 1)
            self.assertTrue(item in h)
            self.assertEqual(h, range(item + 1))

    def test_pop(self) -> None:
        iters = 1000
        h = Heap[int](range(iters))     # type: ignore
        prev = h.pop()
        for exp in range(iters):
            self.assertEqual(prev, exp)
            self.assertEqual(len(h), iters - exp - 1)
            self.assertFalse(prev in h)
            self.assertEqual(h, range(exp + 1, iters))
            if h:
                self.assertEqual(h.peek(), exp + 1)
                prev = h.pop()

    def test_everything(self) -> None:
        iters = 10000
        h = Heap[int]()     # type: ignore      # owing to int not being Comparable
        q = list(h)

        for _ in range(iters):
            if random.randint(0, 1):        # push
                item = random.randint(0, iters)
                h.push(item)
                heapq.heappush(q, item)
                self.assertTrue(item in h)
            elif q:                         # pop
                self.assertEqual(h.peek(), q[0])
                self.assertTrue(q[0] in h)
                item = h.pop()
                self.assertEqual(item, heapq.heappop(q))
            else:
                self.assertRaises(ValueError, h.pop)

            self.assertEqual(len(h), len(q))
            self.assertEqual(h, q)
            self.assertCountEqual(tuple(iter(h)), q)
