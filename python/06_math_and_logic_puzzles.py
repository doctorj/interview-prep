"""
Solutions to "Cracking The Coding Interview" problems on Math and Logic Puzzles.
"""
import unittest
import random
from math import factorial, sqrt, ceil
from typing import Optional


def the_heavy_pill(grams: float, bottles: int = 20) -> int:
    """Given 20 bottles of pills, 19 of which have 1.0 gram pills and 1 of which has 1.1 gram pills,
    and one chance to weigh something, how do you find the heavy bottle?  (6.1)

    :Return: the index of the heavy bottle given the weight of `i` pills from bottle `i` for ``1 <= i < = bottles``."""
    return int(10 * (grams - (bottles * (bottles + 1) // 2)))


def basketball(prob: float) -> int:
    """Choose:  (6.2)

    Game 1: make one shot.
    Game 2: make two out of three shots.

    :param prob: The probability of making a shot.
    :Return: The game with the best chances.

    Analytically, you want to choose Game 1 if::

            P(1) > P(2)
            p > binomial(3, 2, p) + binomial(3, 3, p)       # P(two shots) + P(3 shots); n = 3, k = 2 or 3, p = prob making one shot.
            p > 3!/(2!*1!) * p^2 * (1 - p)^1 + 3!/3! * p^3
            p > 3(p^2 - p^3) + p^3
            p > 3p^2 - 2p^3
            1 > 3p - 2p^2
            0 < 2p^2 - 3p + 1
            0 < (2p - 1)(p - 1)     # must have same sign; p <= 1 implies p - 1 <= 0 implies 2p - 1 <= 0
            0 > 2p - 1
            p < 1/2
    """
    assert 0 <= prob <= 1
    n = 3
    k = 2
    p1 = prob
    p2 = 1 - binomial_cdf(n, k - 1, prob)
    return 1 if p1 > p2 else 2


def dominos() -> None:
    """Can you tile an 8x8 chessboard missing two diagonally opposite corners with 31 (2x1) dominos?  (6.3)

    No: Each domino has to cover one white and one black square.  But, we cut out 2 of the same color,
    leaving 30, so 31 dominos won't fit.
    """
    pass


def ants_on_a_triangle(n: int) -> float:
    """:Return: the probability of a collision if `n` ants on an `n`-sided polygon if each
    ant walks in a random direction.  (6.4)

    The only way to avoid a collision is if they all go the same way, which has probability 1/2^n;
    but there are two ways to choose, so 2/2^n = 1/2^(n-1) = 2^(1-n)
    So the probability of a collision is 1 - 2^(1-n).
    """
    assert n > 1
    return 1 - 2 ** (1 - n)


def jugs_of_water() -> None:
    """Given 5-quart and 3-quart jugs and unlimited water, make exactly 4 quarts of water
    (can't eyball "half" just or the like).  (6.5)

    Fill 5q, pour into 3q, leaving 2.  Empty 3q, pour 2 from 5q into 3q.  Fill 5q.  Pour 1 from 5q into 3q to fill,
    leaving 4 in 5q.

    Oy.  Apparently it's possible to measure any value between 1 and the sum of the jug sizes if the sizes are
    relatively prime.
    """
    pass


def blue_eyed_island() -> None:
    """All blue-eyed residents of an island are ordered to leave ASAP.  The only problem is nobody can
    see their own eye color, and they are not allowed to communicate.  Nobody knows the number of blue-eyed
    people except there is at least 1.  A flight leaves every evening; how many days does it take the blue-eyed
    people to leave?  (6.6)

    Assume there's one blue-eyed person.  He can see no one else has blue eyes, knows there must be at least one,
    so he's it.  He gets on the plane and leaves the first night.

    Assume two blue-eyed people.  Each sees the other, but doesn't know if the other is the only one, or if they
    have blue eyes, too.  But, since the other doesn't leave the first night, they both know they must be blue too,
    and leave the second night.

    Assume b > 2.  Each blue can see b - 1 other blues, but is unsure about himself (unsure whether it's b - 1 or b).
    So they wait b nights, then all leave together on night b.
    """
    pass


def apocalypse() -> float:
    """Simulate the ratio of girls to boys when parents stop at the first girl.  (6.7)"""
    girls = boys = 0
    for _ in range(1000000):
        b = 0
        while random.random() < 0.5:       # boy
            b += 1
        boys += b
        girls += 1

    return girls / boys


def egg_drop(floors: int = 100) -> int:
    """There is a building of 100 `floors`.  An egg dropped from floor N or higher will break,
    below that it will not.  Find N with two eggs while minimizing the number of drops.  (6.8)

    :Return: the number of the floor to start on, incrementing by 1 less that each drop until
    the first break, then linearly until the 2nd.

    First, note that once we break egg 1 say on floor B, we have to probe each floor (increasing)
    from the highest-known safe floor up to B.  So egg 2 probably gets dropped the difference (in
    floors) of egg 1's last two drops, in the worst case.

    Say you try every 10 floors with egg 1.  In the best case, 10 breaks e1, and then you try 1..9
    with e2, or 10 drops total.  But in the worst case, e1 breaks at 100 after 10 drops, and you
    still have 91..99 for e2, or 19 total.

    The idea is to balance e1 and e2 in the worst case.  This means when e1 goes up by 1, e2 must
    go down by 1.  So say e1 starts at floor f; then it goes to f + (f - 1), f + (f - 1) + (f - 2),
    ... 1, and the sum should be 100:

        1 + 2 + ... + f = 100
        f(f + 1) / 2 = 100
        f ~ 13.65
        f = 14 (rounded by examining both cases)
    """
    assert floors > 0 and floors == int(floors)
    start = (-1 + sqrt(1 + 8 * floors)) / 2        # Quadratic formula for x(x+1)/2 = f (other solution is always negative)

    # def drops(start: int, floors: int) -> int:
    #     return start + (floors - start * (start + 1) // 2)

    return int(ceil(start))


def count_egg_drops(floors: int, breaking: int, interval: int) -> Optional[int]:
    """:Return: the number of egg drops needed to find the (lowest) `breaking` floor
    between 1 and `floors` by dropping two eggs according to the procedure in `egg_drop()`.

    Returns None if ``breaking > floors``.
    """
    assert floors > 0
    assert breaking > 0     # Could be > floors
    assert 0 < interval <= floors
    egg1 = interval
    highest_unbroken = 0
    drops = 0

    # Drop egg 1 at decreasing intervals
    while egg1 <= floors and interval > 0:
        drops += 1
        if egg1 >= breaking:
            break       # ha
        highest_unbroken = egg1
        interval -= 1
        egg1 += interval

    # Drop egg 2 at each floor
    egg2 = highest_unbroken + 1
    while egg2 <= floors:
        drops += 1
        if egg2 >= breaking:
            break       # you knew it was coming
        egg2 += 1

    return drops


def one_hundred_lockers(lockers: int) -> int:
    """100 lockers numbered 1 to 100 are initially closed.  After 100 rounds of toggling the state of every ith locker
    on the ith round, how many lockers are open?  (Round 1 opens every locker, round 2 closes every even numbered
    locker... round 100 toggles locker #100).  (6.9)

    First note a locker l is toggled on round i if l is a multiple of i, aka i divides l, aka i is a factor of l.
    So a locker gets toggled once for each factor.  Since it starts closed, it will end up open if its toggled an
    odd number of times.  So locker l where l has an odd number of (distinct) factors will be open at the end.
    Further, factors usually come in pairs (an even number of toggles), unless both factors are the same, which
    only happens if l is a square (and this can only happen once, i.e., sqrt(l) is unique).  So only the squares
    will remain open, and you can count the squares < # lockers.
    """
    n = 1
    while n * n <= lockers:
        n += 1

    return n - 1


def choose(n: int, k: int) -> int:
    """:Return: the binomial coefficient n-choose-k, the number of ways to choose k things from a set of n (without replacement)."""
    assert n >= 0
    assert 0 <= k <= n
    return factorial(n) // (factorial(n - k) * factorial(k))


def binomial(n: int, k: int, p: float) -> float:
    """The binomial distribution mass function, ``choose(n, k) * p**k * (1 - p)**(n - k)``.

    The probability of exactly `k` successes in `n` independent trials with probability `p`.
    """
    return choose(n, k) * p ** k * (1 - p)**(n - k)


def binomial_cdf(n: int, k: int, p: float) -> float:
    """The binomial distribution cumulative density function, the probability of at most `k` successes in
    `n` independent trials of probability `p`."""
    return sum(binomial(n, i, p) for i in range(0, k + 1))


class TestChapter6(unittest.TestCase):
    def test_apocalypse(self) -> None:
        self.assertAlmostEqual(apocalypse(), 1.0, delta=0.01)

    def test_the_heavy_pill(self) -> None:
        self.assertEqual(13, the_heavy_pill(211.3))

    def test_basketbal(self) -> None:
        for _ in range(1000):
            p = random.random()
            self.assertEqual(basketball(p), 1 if p <= 0.5 else 2, p)

    def test_ants_on_a_triangle(self) -> None:
        self.assertEqual(ants_on_a_triangle(3), 3/4)

    def test_egg_drop(self) -> None:
        for floors in range(2, 101):
            computed_interval = egg_drop(floors)
            # interval that minimizes the worst cast number of drops
            empirical_interval = min(range(1, floors // 2 + 1), key=lambda i: max(count_egg_drops(floors, breaking, i) for breaking in range(1, floors + 1)))
            self.assertAlmostEqual(computed_interval, empirical_interval, delta=1)      # Good enough for government work?

    def test_one_hundred_lockers(self) -> None:
        vecs = (
            (1, 1),
            (2, 1),
            (3, 1),
            (4, 2),
            (5, 2),
            (8, 2),
            (9, 3),
            (10, 3),
            (15, 3),
            (16, 4),
            (25, 5),
            (36, 6),
            (49, 7),
            (64, 8),
            (81, 9),
            (99, 9),
            (100, 10),
            (101, 10),
        )
        for lockers, open_ in vecs:
            self.assertEqual(one_hundred_lockers(lockers), open_)
