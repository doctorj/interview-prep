"""
Solutions to "Cracking The Coding Interview" problems on Stacks and Queues.
"""
import random
import unittest
from abc import ABCMeta, abstractmethod
from itertools import islice, product, chain
from typing import TypeVar, Iterable, Iterator, Sequence, Any, overload, List       # pylint: disable=unused-import


class Comparable(metaclass=ABCMeta):                # pylint: disable=too-few-public-methods
    """Abstract base class for objects supporting comparison."""
    @abstractmethod
    def __lt__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __gt__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __le__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

    @abstractmethod
    def __ge__(self, other: Any) -> bool: pass      # pylint: disable=multiple-statements

T = TypeVar('T', bound=Comparable)


def three_in_one():
    """Describe how to implment three stacks with a single array. (3.1)

    Just put the i-th element of stack s in slot 3 * i + s (for s in [0..3)).  This is simple and generalizes to more stacks.
    The question remains why you'd want to do this and what the interface would look like (not pretty)."""


class StackMin(Sequence[T]):
    """A stack that also stores the minimum of all elements. (3.2)"""
    def __init__(self, items: Iterable[T] = ()) -> None:
        """Add `items` to the stack in order (last in, first out)."""
        self.data = []      # type: List[T]
        self.mins = []      # type: List[T]
        self.size = 0
        for item in items:
            self.push(item)

    def push(self, item: T) -> None:
        """Append `item` to the (right) end of the stack."""
        min_ = min(self.min(), item) if self.size else item
        if self.size == len(self.data):
            self.data.append(item)
            self.mins.append(min_)
        else:
            assert self.size < len(self.data)
            self.data[self.size] = item
            self.mins[self.size] = min_
        self.size += 1

    def pop(self) -> T:
        """Remove and return the rightmost item from the stack."""
        if not self.size:
            raise ValueError('Empty Stack')
        self.size -= 1
        return self.data[self.size]

    def min(self) -> T:
        """:Return: the minimum element in the stack.  O(1) time."""
        if self.size:
            return self.mins[self.size - 1]
        else:
            raise ValueError('Empty Stack')

    @overload       # These are just for the type checker, and overwritten by the real implementation
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is first in and -1 is last in."""
        if isinstance(index, int):
            if 0 <= index < self.size:
                return self.data[index]
            elif -self.size <= index < 0:
                return self.data[self.size + index]
            else:
                raise ValueError('Index ({}) out of range ({})'.format(index, self.size))

        elif isinstance(index, slice):
            return self.data[:self.size][index]     # Ok this is a bit cheezy... but easy!
        else:
            raise TypeError('Invalid index type')

    def __len__(self) -> int:
        return self.size

    def __iter__(self) -> Iterator[T]:
        """:Return: an iterator of the items from left (first in) to right (last in)."""
        return islice(self.data, self.size)

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False

    def __repr__(self) -> str:
        return 'StackMin({})'.format(tuple(self))


class StackOfPlates(Sequence[T]):
    """A stack implemented with multiple bounded-size stacks.  (3.3)"""
    def __init__(self, items: Iterable[T] = (), capacity: int = 1024) -> None:
        """Add `items` to the stack in order (last in, first out).

        :param capacity: The maximum size of an individual internal stack.
        """
        self.stacks = []        # type: List[List[T]]
        self.size = 0
        self.capacity = capacity
        for item in items:
            self.push(item)

    def push(self, item: T) -> None:
        """Append `item` to the (right) end of the stack."""
        if self.size // self.capacity >= len(self.stacks):
            self.stacks.append([])
        self.stacks[self.size // self.capacity].append(item)
        self.size += 1

    def pop(self) -> T:
        """Remove and return the rightmost item from the stack."""
        if not self.size:
            raise ValueError('Empty Stack')
        self.size -= 1
        return self.stacks[self.size // self.capacity].pop()

    @overload       # These are just for the type checker, and overwritten by the real implementation
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is first in and -1 is last in."""
        if isinstance(index, int):
            if index < 0:
                index += self.size
            if 0 <= index < self.size:
                return self.stacks[index // self.capacity][index % self.capacity]
            else:
                raise ValueError('Index ({}) out of range ({})'.format(index, self.size))

        elif isinstance(index, slice):
            return list(self)[index]     # Ok this really cheezy... but easy!
        else:
            raise TypeError('Invalid index type')

    def __len__(self) -> int:
        return self.size

    def __iter__(self) -> Iterator[T]:
        """:Return: an iterator of the items from left (first in) to right (last in)."""
        return islice(chain.from_iterable(self.stacks), self.size)

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False

    def __repr__(self) -> str:
        return 'StackOfPlates({})'.format(tuple(self))


class StaQueue(Sequence[T]):
    """Queue implemented with two stacks.  (3.4)"""
    def __init__(self, items: Iterable[T] = ()) -> None:
        self.main = list(items)
        self.aux = []       # type: List[T]

    def push(self, item: T) -> None:
        """Append `item` to the right of the queue."""
        self.main.append(item)

    def pop(self) -> T:
        """Remove and return the leftmost item from the queue."""
        if not self:
            raise ValueError('Empty Queue')
        while self.main:
            self.aux.append(self.main.pop())
        item = self.aux.pop()
        while self.aux:
            self.main.append(self.aux.pop())
        return item

    @overload
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is left (first in) and -1 is right (last in)."""
        if isinstance(index, int):
            if index < 0:
                index += len(self)
            if 0 <= index < len(self):
                return self.main[index]
            else:
                raise IndexError('Index ({}) out of range ({})'.format(index, len(self)))
        elif isinstance(index, slice):
            return list(self)[index]        # Ineffecient but easily captures Python's slicing semantics, which islice does not.
        else:
            raise TypeError('Invalid index type')

    def __len__(self) -> int:
        return len(self.main)

    def __iter__(self) -> Iterator[T]:
        """Iterate over items from left (first in) to right (last in)."""
        return islice(self.main, len(self))

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False


def sort_stack(s):
    """Sort list/stack `s` in descending order (smallest on top) using an auxiliary stack.  O(n^2).  (3.5)"""
    aux = []        # using list as stack so we don't depend on other code
    while s:
        item = s.pop()
        while aux and aux[-1] > item:
            s.append(aux.pop())
        aux.append(item)

    while aux:
        s.append(aux.pop())


class StackMinTest(unittest.TestCase):
    iters = 100

    def test_init(self) -> None:
        self.assertTupleEqual(tuple(iter(StackMin[int](range(self.iters)))), tuple(range(self.iters)))
        self.assertTupleEqual(tuple(StackMin[int](range(self.iters))), tuple(range(self.iters)))
        self.assertSequenceEqual(StackMin[int](range(self.iters)), tuple(range(self.iters)))

    def test_push(self) -> None:
        items = tuple(range(self.iters))
        s = StackMin[int]()
        for idx, item in enumerate(items):
            self.assertEqual(idx, len(s))
            s.push(item)
            self.assertEqual(idx + 1, len(s))
            self.assertSequenceEqual(s, items[:idx + 1])
            self.assertEqual(s[-1], item)

    def test_pop(self) -> None:
        items = tuple(range(self.iters))
        s = StackMin[int](items)
        for idx in reversed(range(self.iters)):
            self.assertEqual(idx + 1, len(s))
            self.assertEqual(s.pop(), items[idx])
            self.assertEqual(idx, len(s))
            self.assertSequenceEqual(s, items[:idx])
            if idx:
                self.assertEqual(s[-1], items[idx - 1])

    def test_min(self) -> None:
        size = 100
        items = tuple(random.randrange(size) for _ in range(size))
        s = StackMin[int]()
        self.assertRaises(ValueError, s.min)
        for idx, item in enumerate(items):
            s.push(item)
            self.assertEqual(s.min(), min(items[:idx + 1]))
        for idx in range(len(items) - 1, 0, -1):
            self.assertEqual(s.min(), min(items[:idx + 1]))
            s.pop()

    def test_getitem(self) -> None:
        items = tuple(range(self.iters))
        s = StackMin[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(s[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(s[i1:i2], list(items[i1:i2]))

    def test_everything(self) -> None:
        iters = 10000
        s = StackMin[int]()
        L = []
        for item in range(iters):
            if random.randint(0, 1):
                s.push(item)
                L.append(item)
            elif L:
                self.assertEqual(s[-1], L[-1])
                self.assertEqual(s.min(), min(L))
                self.assertEqual(s.pop(), L.pop())
            else:
                self.assertRaises(ValueError, s.pop)
                self.assertRaises(ValueError, s.min)

            self.assertEqual(len(s), len(L))
            self.assertSequenceEqual(s, L)


class StackOfPlatesTest(unittest.TestCase):
    iters = 101

    def test_init(self) -> None:
        self.assertTupleEqual(tuple(iter(StackOfPlates[int](range(self.iters), 17))), tuple(range(self.iters)))
        self.assertTupleEqual(tuple(StackOfPlates[int](range(self.iters), 42)), tuple(range(self.iters)))
        self.assertSequenceEqual(StackOfPlates[int](range(self.iters)), tuple(range(self.iters)))

    def test_push(self) -> None:
        items = tuple(range(self.iters))
        s = StackOfPlates[int]()
        for idx, item in enumerate(items):
            self.assertEqual(idx, len(s))
            s.push(item)
            self.assertEqual(idx + 1, len(s))
            self.assertSequenceEqual(s, items[:idx + 1])
            self.assertEqual(s[-1], item)

    def test_pop(self) -> None:
        items = tuple(range(self.iters))
        s = StackOfPlates[int](items)
        for idx in reversed(range(self.iters)):
            self.assertEqual(idx + 1, len(s))
            self.assertEqual(s.pop(), items[idx])
            self.assertEqual(idx, len(s))
            self.assertSequenceEqual(s, items[:idx])
            if idx:
                self.assertEqual(s[-1], items[idx - 1])

    def test_getitem(self) -> None:
        items = tuple(range(self.iters))
        s = StackOfPlates[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(s[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(s[i1:i2], list(items[i1:i2]))

    def test_everything(self) -> None:
        iters = 10000
        s = StackOfPlates[int]()
        L = []
        for item in range(iters):
            if random.randint(0, 1):
                s.push(item)
                L.append(item)
            elif L:
                self.assertEqual(s[-1], L[-1])
                self.assertEqual(s.pop(), L.pop())
            else:
                self.assertRaises(ValueError, s.pop)

            self.assertEqual(len(s), len(L))
            self.assertSequenceEqual(s, L)


class StaQueueTest(unittest.TestCase):
    iters = 100

    def test_init(self) -> None:
        self.assertListEqual(list(StaQueue()), [])
        self.assertListEqual(list(StaQueue((1,))), [1])
        self.assertListEqual(list(StaQueue(range(10))), list(range(10)))
        self.assertNotEqual(StaQueue(range(10)), list(range(9)))
        self.assertNotEqual(StaQueue(range(9)), list(range(10)))
        self.assertNotEqual(StaQueue(), list(range(1)))
        self.assertNotEqual(StaQueue(range(9)), object())

    def test_push(self) -> None:
        q, L = StaQueue[int](), list()
        for idx in range(self.iters):
            q.push(idx)
            L.append(idx)
            self.assertEqual(len(q), len(L))
            self.assertEqual(q[-1], L[-1])
            self.assertSequenceEqual(q, L)
            self.assertListEqual(list(iter(q)), L)

    def test_pop(self) -> None:
        L = list(range(self.iters))
        q = StaQueue(L)
        for _ in range(self.iters):
            self.assertEqual(q.pop(), L.pop(0))
            self.assertEqual(len(q), len(L))
            self.assertSequenceEqual(q, L)
            self.assertListEqual(list(iter(q)), L)
            if L:
                self.assertEqual(q[-1], L[-1])

    def test_getitem(self) -> None:
        items = tuple(range(self.iters))
        q = StaQueue[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(q[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(q[i1:i2], list(items[i1:i2]))

    def test_everything(self) -> None:
        iters = 10000
        q = StaQueue[int]()
        L = []
        for item in range(iters):
            if random.randint(0, 1):
                q.push(item)
                L.append(item)
            elif L:
                self.assertEqual(q[-1], L[-1])
                self.assertEqual(q.pop(), L.pop(0))
            else:
                self.assertRaises(ValueError, q.pop)

            self.assertEqual(len(q), len(L))
            self.assertSequenceEqual(q, L)


class TestChapter3(unittest.TestCase):
    iters = 100

    def test_sort_stack(self):
        for _ in range(self.iters):
            s = [random.randrange(self.iters) for _ in range(self.iters)]
            exp = sorted(s, reverse=True)
            sort_stack(s)
            self.assertListEqual(s, exp)
