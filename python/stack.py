"""
Basic list-based stack implementation.

Ignores Python lists' stack functionality.
The stack capacity grows but never shrinks.
"""
import random
import unittest
from itertools import islice, product
from typing import TypeVar, Iterable, Iterator, Sequence, Any, overload

T = TypeVar('T')


class Stack(Sequence[T]):
    def __init__(self, items: Iterable[T] = ()) -> None:
        """Add `items` to the stack in order (last in, first out)."""
        self.data = list(items)
        self.size = len(self.data)

    def push(self, item: T) -> None:
        """Append `item` to the (right) end of the stack."""
        if self.size == len(self.data):
            self.data.append(item)
        else:
            assert self.size < len(self.data)
            self.data[self.size] = item
        self.size += 1

    def pop(self) -> T:
        """Remove and return the rightmost item from the stack."""
        if not self.size:
            raise ValueError('Empty Stack')
        self.size -= 1
        return self.data[self.size]

    @overload       # These are just for the type checker, and overwritten by the real implementation
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is first in and -1 is last in."""
        if isinstance(index, int):
            if 0 <= index < self.size:
                return self.data[index]
            elif -self.size <= index < 0:
                return self.data[self.size + index]
            else:
                raise ValueError('Index ({}) out of range ({})'.format(index, self.size))

        elif isinstance(index, slice):
            return self.data[:self.size][index]     # Ok this is a bit cheezy... but easy!
        else:
            raise TypeError('Invalid index type')

    def __len__(self) -> int:
        return self.size

    def __iter__(self) -> Iterator[T]:
        """:Return: an iterator of the items from left (first in) to right (last in)."""
        return islice(self.data, self.size)

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False

    def __repr__(self) -> str:
        return 'Stack({})'.format(tuple(self))


class StackTest(unittest.TestCase):
    def test_init(self) -> None:
        self.assertTupleEqual(tuple(iter(Stack[int](range(10)))), tuple(range(10)))
        self.assertTupleEqual(tuple(Stack[int](range(10))), tuple(range(10)))
        self.assertSequenceEqual(Stack[int](range(10)), tuple(range(10)))

    def test_push(self) -> None:
        items = tuple(range(10))
        s = Stack[int]()
        for idx, item in enumerate(items):
            self.assertEqual(idx, len(s))
            s.push(item)
            self.assertEqual(idx + 1, len(s))
            self.assertSequenceEqual(s, items[:idx + 1])
            self.assertEqual(s[-1], item)

    def test_pop(self) -> None:
        items = tuple(range(10))
        s = Stack[int](items)
        for idx in reversed(range(10)):
            self.assertEqual(idx + 1, len(s))
            self.assertEqual(s.pop(), items[idx])
            self.assertEqual(idx, len(s))
            self.assertSequenceEqual(s, items[:idx])
            if idx:
                self.assertEqual(s[-1], items[idx - 1])

    def test_getitem(self) -> None:
        items = tuple(range(10))
        s = Stack[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(s[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(list(s[i1:i2]), list(items[i1:i2]))

    def test_everything(self) -> None:
        iters = 10000
        s = Stack[int]()
        L = []
        for item in range(iters):
            if random.randint(0, 1):
                s.push(item)
                L.append(item)
            elif L:
                self.assertEqual(s[-1], L[-1])
                self.assertEqual(s.pop(), L.pop())
            else:
                self.assertRaises(ValueError, s.pop)

            self.assertEqual(len(s), len(L))
            self.assertSequenceEqual(s, L)
