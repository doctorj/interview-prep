"""
A basic hashtable with separate chaining.

Implementation is a list of lists storing (key, value) tuples.
Primary list grows but never shrinks.
"""
import statistics
import random
import unittest
from typing import List, Any, Mapping, MutableMapping, TypeVar, Optional, Tuple, Iterator, Dict


K = TypeVar('K')
V = TypeVar('V')
Item = Tuple[K, V]
# As of 3/9/17, mypy 0.5, Python does not have a functioning notion of Hashable.
# https://github.com/python/mypy/issues/1746


class Hashtable(MutableMapping[K, V], Mapping[K, V]):       # pylint: disable=too-few-public-methods
    DEFAULT_CAPACITY = 16       #: Initial capacity
    GROWTH_FACTOR = 2.0         #: Increase size by this factor when loadfactor exceeded

    def __init__(self, capacity: int = DEFAULT_CAPACITY, loadfactor: float = 0.75) -> None:
        assert capacity > 0
        assert loadfactor * capacity > 1
        self.data = [[] for _ in range(capacity)]   # type: List[List[Item]]
        self.size = 0
        self.loadfactor = loadfactor

    def _slot(self, key: K, capacity: Optional[int] = None) -> int:
        """:Return: the index of `key` in a table of `capacity` (or current capacity if None)."""
        return hash(key) % (capacity or len(self.data))

    @staticmethod
    def _index(key: K, slot: List[Item]) -> Optional[int]:
        """:Return: the index of `key` in list `slot`, or None if not in the Hashtable."""
        for idx, (k, _) in enumerate(slot):
            if k == key:
                return idx
        return None

    def __getitem__(self, key: K) -> V:
        for k, v in self.data[self._slot(key)]:
            if k == key:
                return v
        raise KeyError(str(key))

    def __setitem__(self, key: K, value: V) -> None:
        slot = self.data[self._slot(key)]
        idx = self._index(key, slot)        # Get index if key already present, None if not
        if idx is not None:                 # If already present, replace value
            assert idx < len(slot)
            slot[idx] = (key, value)
        else:                               # If not present, add to end
            slot.append((key, value))
            self.size += 1
            if self.size > len(self.data) * self.loadfactor:
                self._rehash()

    def __delitem__(self, key: K) -> None:
        slot = self.data[self._slot(key)]
        idx = self._index(key, slot)
        if idx is None:     # Not present
            raise KeyError(key)
        else:
            slot.pop(idx)
            self.size -= 1

    def _rehash(self, newsize: Optional[int] = None) -> None:
        """Change the size of the internal table and re-hash all elements into it."""
        newsize = int(newsize or len(self.data) * self.GROWTH_FACTOR)
        assert newsize > 1
        new = [[] for _ in range(newsize)]      # type: List[List[Item]]
        for k, v in self.items():
            new[self._slot(k, newsize)].append((k, v))      # Assuming no dupes
        self.data = new

    def __len__(self) -> int:
        return self.size

    def __iter__(self) -> Iterator[K]:
        return (k for slot in self.data for k, _ in slot)

    def _loadstats(self) -> Dict[str, float]:
        """:Return: a dictionary of load statistics."""
        slot_sizes = tuple(len(slot) for slot in self.data)
        return {
            'slots': len(self.data),
            'size': len(self),
            'load': len(self) / len(self.data),
            'slots_occupied': sum(1 for size in slot_sizes if size),
            'slot_size_std': statistics.stdev(slot_sizes),
            'slot_size_max': max(slot_sizes),
        }


class HashtableTest(unittest.TestCase):
    ITERS = 1000

    def test_insert(self) -> None:
        h, d = Hashtable[Any, Any](), dict()
        k, v = 0, None      # type: Any, Any
        for k in range(self.ITERS):
            h[k] = d[k] = 2 * k
            self.assertEqual(h[k], d[k])
            self.assertEqual(h, d)
            self.assertEqual(len(h), len(d))
        for k in range(self.ITERS):     # Test dupes
            h[k] = d[k] = 2 * k
            self.assertEqual(h[k], d[k])
            self.assertEqual(h, d)
            self.assertEqual(len(h), len(d))

        h, d = Hashtable(), dict()
        for k in range(self.ITERS):
            k, v = str(k), str(2 * k)
            h[k] = d[k] = v
            self.assertEqual(h[k], d[k])
            self.assertEqual(h, d)

        h, d = Hashtable(), dict()
        for _ in range(self.ITERS):
            k, v = object(), object()
            h[k] = d[k] = v
            self.assertEqual(h[k], d[k])
            self.assertEqual(h, d)

        h, d = Hashtable(), dict()
        for _ in range(self.ITERS):
            k, v = random.randint(-self.ITERS, self.ITERS), random.randint(-self.ITERS, self.ITERS)
            h[k] = d[k] = v
            self.assertEqual(h[k], d[k])
            self.assertEqual(h, d)

    def test_remove(self) -> None:
        h, d = Hashtable[int, int](), dict()
        for k in range(self.ITERS):
            h[k] = d[k] = 2 * k

        for _ in range(self.ITERS):
            k, v = d.popitem()
            self.assertEqual(h[k], v)
            del h[k]
            self.assertEqual(len(h), len(d))
            self.assertEqual(h, d)

    def test_iter(self) -> None:
        h, d = Hashtable[int, int](), dict()
        for k in range(self.ITERS):
            h[k] = d[k] = 2 * k

        self.assertSetEqual(set(iter(h)), set(iter(d)))

    def test_everything(self) -> None:
        INSERT, REMOVE, LOOKUP = 0, 1, 2
        h, d = Hashtable[int, int](), dict()
        for _ in range(3 * self.ITERS):
            op = random.randint(0, 2)
            if op == INSERT:
                k, v = random.randint(-self.ITERS, self.ITERS), random.randint(-self.ITERS, self.ITERS)
                h[k] = d[k] = v
                self.assertEqual(h[k], v)
            elif op == REMOVE:
                if d:
                    k, v = d.popitem()
                    self.assertEqual(h[k], v)
                    del h[k]
            elif op == LOOKUP:
                if d:
                    k, v = d.popitem()
                    self.assertEqual(h[k], v)
                    d[k] = v
            else:
                assert False

            self.assertEqual(len(h), len(d))
            self.assertEqual(h, d)

    def test_loadfactor(self) -> None:
        for loadfactor in (0.1, 0.5, 0.75, 0.9, 1.1, 2.0):
            h = Hashtable[int, int](loadfactor=loadfactor)
            for i in range(int(Hashtable.DEFAULT_CAPACITY * Hashtable.GROWTH_FACTOR ** 5)):
                h[i] = i
                self.assertLessEqual(len(h) / len(h.data), loadfactor)
