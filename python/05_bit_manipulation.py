"""
Solutions to "Cracking The Coding Interview" problems on Bit Manipulation.
"""
import random
import unittest
from itertools import product
from typing import List


def insertion(n: int, m: int, i: int, j: int) -> int:
    """Insert `m` into `n` such that `m` starts at high bit `j` and ends at low bit `i` inclusive.  (5.1)"""
    assert j >= i
    clear = ~(((1 << (j - i + 1)) - 1) << i)        # zero bits j..i inclusive
    return n & clear | (m << i)                     # OR in m


def binary_to_string(val: float, bits: int = 32) -> str:
    """:Return: Floating-point `val` in [0, 1] represented as a `bits`-bit binary string.  (5.2)"""
    assert 0 <= val < 1
    assert 0 < bits < 1e6 and bits == int(bits)
    s = '0.'
    n = int(val * 2**bits)
    if n != val * 2**bits:
        raise ValueError('Cannot represent {} with {} bits'.format(val, bits))
    # Always do at least one zero after the binary point
    s += '1' if (n & (1 << (bits - 1))) else '0'
    n = (n << 1) & ((1 << bits) - 1)
    while n > 0:
        s += '1' if (n & (1 << (bits - 1))) else '0'
        n = (n << 1) & ((1 << bits) - 1)
    return s


def flip_bit_to_win(val: int, bits: int = 32) -> int:
    """:Return: the length of the longest sequence of 1s you can obtain in the binary represetnation of `val`
    by flipping one bit.  (5.3)"""
    def one_length(n: int, bits: int = 32) -> int:
        maxrun, run = 0, 0
        for b in range(bits):
            if n & (1 << b):
                run += 1
                maxrun = max(maxrun, run)
            else:
                run = 0
        return maxrun

    return max(one_length(val | (1 << b)) for b in range(bits))


def conversion(a: int, b: int) -> int:
    """:Return: the number of bits needed to flip to turn `a` into `b`.  (5.6)"""
    return bin(a ^ b).count('1')


def pairwise_swap(val: int) -> int:
    """:Return: `val` with bit 0 and 1 swapped, bit 2 and 3 swapped, ...  (5.7)"""
    mask = 0x55555555
    return ((mask & val) << 1) | (((mask << 1) & val) >> 1)


def draw_line(screen: List[int], width: int, x1: int, x2: int, y: int) -> None:
    """Draw a horizontal line on 1-bit packed `screen` from `x1` to `x2` at height `y`.  (5.8)"""
    assert width % 8 == 0

    def set_pixel(xx: int, yy: int) -> None:
        screen[(width * yy + xx) // 8] |= 1 << (xx % 8)

    for x in range(x1, x2 + 1):
        set_pixel(x, y)


class TestChapter5(unittest.TestCase):
    def test_insertion(self) -> None:
        vecs = (    # dest, src, lo, hi, result
            (0, 1, 0, 0, 1),
            (0, 0b11, 0, 1, 0b11),
            (0b11, 0b11, 0, 2, 0b11),
            (0b0101, 0b1010, 0, 4, 0b1010),
            (0b010101, 1, 1, 1, 0b010111),
            (0b11111111, 0, 3, 3, 0b11110111),
            (0b11111111, 0, 3, 6, 0b10000111),
            (0, 0b11111111, 0, 7, 0b11111111),
            (0b10000000000, 0b10011, 2, 6, 0b10001001100),
        )
        for n, m, i, j, expected in vecs:
            self.assertEqual(insertion(n, m, i, j), expected)

    def test_binary_to_string(self) -> None:
        vecs = (
            (0, '0.0'),
            (0.5, '0.1'),
            (0.25, '0.01'),
            (0.125, '0.001'),
            (0.75, '0.11'),
            (0.625, '0.101'),
            (0.625, '0.101'),
            (0.25 + 0.0625, '0.0101'),
            (2 ** -31, '0.' + '0' * 30 + '1'),
        )
        for val, rep in vecs:
            self.assertEqual(binary_to_string(val), rep)

    def test_flip_bit_to_win(self) -> None:
        vecs = (
            (0, 1),
            (1, 2),
            (2, 2),
            (3, 3),
            (0b101, 3),
            (0b1101, 4),
            (0b01110011110, 5),
            (1775, 8),
        )
        for val, length in vecs:
            self.assertEqual(flip_bit_to_win(val), length)

    def test_conversion(self) -> None:
        vecs = (
            (0, 0, 0),
            (0, 1, 1),
            (1, 1, 0),
            (0, 2, 1),
            (0, 3, 2),
            (3, 0, 2),
            (0b0101, 0b1010, 4),
            (29, 15, 2),
        )
        for a, b, count in vecs:
            self.assertEqual(conversion(a, b), count)

    def test_pairwise_swap(self) -> None:
        vecs = (
            (0, 0),
            (1, 0b10),
            (2, 0b01),
            (3, 0b11),
            (0b0101, 0b1010),
            (0b101010, 0b010101),
            (0b100, 0b1000),
        )
        for a, exp in vecs:
            self.assertEqual(pairwise_swap(a), exp)

    def test_draw_line(self) -> None:
        size = 64

        def get_pixel(screen: List[int], width: int, x: int, y: int) -> bool:
            return bool(screen[(width * y + x) // 8] & (1 << (x % 8)))

        def draw_screen(screen: List[int], width: int) -> None:     # pylint: disable=unused-variable
            print('\n{} x {} screen'.format(width, len(screen) // width // 8))
            y = 0
            while y * width // 8 < len(screen):
                print('{:3d}  {}'.format(y, ''.join('X' if get_pixel(screen, width, x, y) else ' ' for x in range(width))))
                y += 1

        for _ in range(100):
            width, height = random.randrange(1, size // 8) * 8, random.randrange(1, size)
            x1 = random.randrange(width)
            x2 = random.randrange(x1, width)
            y = random.randrange(height)
            screen = [0] * (width * height // 8)
            draw_line(screen, width, x1, x2, y)
            for xx, yy in product(range(width), range(height)):
                self.assertEqual(get_pixel(screen, width, xx, yy), yy == y and (x1 <= xx <= x2))
