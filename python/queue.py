"""
Queue based on a ring buffer based on a list.
"""

import random
import unittest
from itertools import product, chain, repeat
from typing import TypeVar, Iterable, Iterator, Sequence, overload, Optional, Any

T = TypeVar('T')


class Queue(Sequence[T]):
    """Simple list-based FIFO queue.  Items are pushed on the right and popped from the left."""
    def __init__(self, items: Iterable[T] = ()) -> None:
        self.data = list(items)
        self.left = 0                   # left % len(self.data) is index of left (first in) item
        self.right = len(self.data)     # right % len(self.data) - 1 is index of right (first out); also the index that you will "write"

    def push(self, item: T) -> None:
        """Append `item` to the right of the queue."""
        assert 0 <= self.left <= self.right
        if self.right - self.left >= len(self.data):
            self._resize()
        self.data[self.right % len(self.data)] = item
        self.right += 1

    def pop(self) -> T:
        """Remove and return the leftmost item from the queue."""
        assert 0 <= self.left <= self.right
        if not self:
            raise ValueError('Empty Queue')
        item = self.data[self.left % len(self.data)]
        self.left += 1
        return item

    def _resize(self, size: Optional[int] = None) -> None:
        assert 0 <= self.left <= self.right
        if not size:
            size = 2 * (len(self.data) + 1)     # + 1 because we initially start empty
        oldsize = len(self)
        assert size >= oldsize, 'New size cannot be smaller than existing size'
        self.data = list(chain(iter(self), repeat(None, size - oldsize)))       # type: ignore # You could copy in-place and extend for a maybe performance boost
        self.left = 0
        self.right = oldsize

    @overload
    def __getitem__(self, index: int) -> T:
        pass

    @overload
    def __getitem__(self, index: slice) -> Sequence[T]:     # pylint: disable=function-redefined
        pass

    def __getitem__(self, index):       # type: ignore      # pylint: disable=function-redefined
        """:Return: the item(s) at the given `index`, which can be a :func:`slice`; 0 is left (first in) and -1 is right (last in)."""
        if isinstance(index, int):
            if 0 <= index < len(self):
                return self.data[(self.left + index) % len(self.data)]
            elif -len(self) <= index < 0:
                return self.data[(self.right + index) % len(self.data)]
            else:
                raise IndexError('Index ({}) out of range ({})'.format(index, len(self)))
        elif isinstance(index, slice):
            return list(self)[index]        # Ineffecient but easily captures Python's slicing semantics, which islice does not.
        else:
            raise TypeError('Invalid index type')

    def __len__(self) -> int:
        assert 0 <= self.left <= self.right
        return self.right - self.left

    def __iter__(self) -> Iterator[T]:
        """Iterate over items from left (first in) to right (last in)."""
        assert 0 <= self.left <= self.right
        return (self.data[idx % len(self.data)] for idx in range(self.left, self.right))

    def __eq__(self, other: Any) -> bool:
        try:
            return len(self) == len(other) and all(us == them for us, them in zip(iter(self), iter(other)))
        except TypeError:       # For no len() or iter()
            return False


class QueueTest(unittest.TestCase):
    def test_init(self) -> None:
        self.assertListEqual(list(Queue()), [])
        self.assertListEqual(list(Queue((1,))), [1])
        self.assertListEqual(list(Queue(range(10))), list(range(10)))
        self.assertNotEqual(Queue(range(10)), list(range(9)))
        self.assertNotEqual(Queue(range(9)), list(range(10)))
        self.assertNotEqual(Queue(), list(range(1)))
        self.assertNotEqual(Queue(range(9)), object())

    def test_push(self) -> None:
        iters = 1000
        q, L = Queue[int](), list()
        for idx in range(iters):
            q.push(idx)
            L.append(idx)
            self.assertEqual(len(q), len(L))
            self.assertEqual(q[-1], L[-1])
            self.assertSequenceEqual(q, L)
            self.assertListEqual(list(iter(q)), L)

    def test_pop(self) -> None:
        iters = 1000
        L = list(range(iters))
        q = Queue(L)
        for _ in range(iters):
            self.assertEqual(q.pop(), L.pop(0))
            self.assertEqual(len(q), len(L))
            self.assertSequenceEqual(q, L)
            self.assertListEqual(list(iter(q)), L)
            if L:
                self.assertEqual(q[-1], L[-1])

    def test_getitem(self) -> None:
        items = tuple(range(10))
        q = Queue[int](items)
        for idx, item in enumerate(items):
            self.assertEqual(q[idx], item)
        for i1, i2 in product(range(-len(items) - 2, len(items) + 2), repeat=2):
            self.assertListEqual(list(q[i1:i2]), list(items[i1:i2]))

    def test_everything(self) -> None:
        iters = 10000
        q = Queue[int]()
        L = []
        for item in range(iters):
            if random.randint(0, 1):
                q.push(item)
                L.append(item)
            elif L:
                self.assertEqual(q[-1], L[-1])
                self.assertEqual(q.pop(), L.pop(0))
            else:
                self.assertRaises(ValueError, q.pop)

            self.assertEqual(len(q), len(L))
            self.assertSequenceEqual(q, L)
