"""
Solutions to "Cracking The Coding Interview" problems on Trees and Graphs.
"""
import random
import unittest
from itertools import chain, zip_longest, product
from math import log
from typing import Generic, TypeVar, Optional, Iterable, Sequence, Tuple, List, Any, Dict, Callable, Set, Union     # pylint: disable=unused-import

from graph import Graph

T = TypeVar('T')
V = TypeVar('V')


class Node(Generic[T], object):
    """Generic tree node.  Can handle any number of children, with convenience properties `left` and `right` for binary trees."""
    __slots__ = ('data', 'children', 'parent')

    def __init__(self, data: T, *children: Optional['Node[T]']) -> None:
        """Create a new node with the given `data` value and any number of `children` (in order from left to right)."""
        self.data = data
        self.children = list(children)
        self.parent = None      # type: Optional['Node[T]']

    @property
    def left(self) -> Optional['Node[T]']:
        return self.children[0] if self.children else None

    @left.setter
    def left(self, value: Optional['Node[T]']) -> None:
        if self.children:
            self.children[0] = value
        else:
            self.children.append(value)

    @property
    def right(self) -> Optional['Node[T]']:
        return self.children[1] if len(self.children) >= 2 else None

    @right.setter
    def right(self, value: Optional['Node[T]']) -> None:
        if len(self.children) <= 1:
            self.children.extend([None] * (2 - len(self.children)))
        self.children[1] = value

    def __eq__(self, other: Any) -> bool:
        return other is not None and self.data == other.data and all(mine == theirs for mine, theirs in zip_longest(self.children, other.children))

    def __str__(self) -> str:
        return ("{} : [{}]\n".format(self.data, ', '.join(map(str, (None if child is None else child.data for child in self.children)))) +
                '\n'.join(str(child) for child in self.children if child is not None))


def route_between_nodes(graph: Graph[V], v1: V, v2: V) -> bool:
    """:Return: True iff there is a directed path between vertices `v1` and `v2` in `graph`.  (4.1)"""
    return graph.is_path(v1, v2)


def minimal_tree(values: Sequence[int]) -> Optional[Node[int]]:
    """:Return: a BST with minimal height created from unique sorted integers `values`.  (4.2)"""
    if not values:
        return None
    if len(values) == 1:
        return Node(values[0])
    median = len(values) // 2
    return Node(values[median], minimal_tree(values[:median]), minimal_tree(values[median + 1:]))


def _dfs(tree: Node[T], visit: Callable[[Node[T], int], bool], depth: int = 0) -> bool:
    """Depth first search of `tree`.  Calls ``visit(node, depth)`` on each node until `visit` returns True.

    :Returns: True if a call to `visit()` did."""
    for child in filter(lambda c: c is not None, tree.children):
        assert child is not None        # For typechecker
        if _dfs(child, visit, depth + 1):
            return True
    return visit(tree, depth)


def list_of_depths(tree: Node[T]) -> List[List[T]]:
    """:Return: a map of depth to list of data at that depth for a binary `tree`.  (4.3)"""
    levels = [[]]       # type: List[List[T]]

    def visit(node: Node[T], depth: int) -> bool:
        while len(levels) <= depth:
            levels.append([])
        levels[depth].append(node.data)
        return False

    _dfs(tree, visit)
    return levels


def _height_if_balanced(node: Optional[Node[T]]) -> Optional[int]:
    """:Return: the length of the longest path (in nodes inclusive) from `node` to a leaf, or None if the heights of
    the subtrees of any node differ by more than 1."""
    if node is None:
        return 0
    lht, rht = _height_if_balanced(node.left), _height_if_balanced(node.right)
    if lht is None or rht is None or abs(lht - rht) > 1:
        return None
    return max(lht, rht) + 1


def check_balanced(tree: Node[T]) -> bool:
    """:Return: True if the heights of the subtrees of every node differ by at most 1.  (4.4)"""
    # The straightforward solution is O(N log N), this is O(N).
    return _height_if_balanced(tree) is not None


def validate_bst(tree: Optional[Node[int]], min_: Optional[int] = None, max_: Optional[int] = None) -> bool:
    """:Return: True if `tree` is a binary search tree."""
    return (tree is None or
            all((min_ is None or tree.data >= min_,
                 max_ is None or tree.data <= max_,
                 validate_bst(tree.left, min_, tree.data),
                 validate_bst(tree.right, tree.data, max_))))


def _make_tree(links: Iterable[Tuple[Optional[int], Optional[int], Optional[int]]]) -> Node[int]:
    """:Return: a Node (tree) created from ``(parent, left, right)`` triples."""
    nodes = dict()      # type: Dict[int, Node[int]]
    not_root = set()
    for parent, left, right in links:
        assert parent is not None
        lchild, rchild = None, None
        if left is not None:
            lchild = nodes.setdefault(left, Node(left))
            not_root.add(left)
        if right is not None:
            rchild = nodes.setdefault(right, Node(right))
            not_root.add(right)
        pnode = nodes.setdefault(parent, Node(parent))
        pnode.left = lchild
        pnode.right = rchild

    roots = set(nodes.keys()) - not_root
    if len(roots) != 1:
        raise ValueError('Graph is not a tree ({} roots)'.format(len(roots)))
    return nodes[roots.pop()]


def _add_parents(node: Optional[Node[T]]) -> None:
    """Add parent pointers to an existing tree `node`."""
    if node is not None:
        for child in node.children:
            if child is not None:
                _add_parents(child)
                child.parent = node


def height(node: Optional[Node[T]]) -> int:
    """:Return: the length (in nodes inclusive) of the longest path from `node` to a leaf."""
    if node is None:
        return 0
    return max((height(child) for child in node.children), default=0) + 1


def successor(node: Node[T]) -> Optional[Node[T]]:
    """:Return: the successor of `node` in a BST with parent pointers.  (4.6)"""
    # If you can go right, go right once, then left as far as you can go.
    # Else, while you're a right child, go up.
    if node.right:
        succ = node.right
        while succ.left:
            succ = succ.left
        return succ
    else:
        succ = node
        while succ.parent and succ == succ.parent.right:
            succ = succ.parent
        return succ.parent


def build_order(projects: Sequence[T], deps: Sequence[Tuple[T, T]]) -> List[T]:
    """:Return: a valid build order for a list of `projects` with a list of dependencies `deps`.  (4.7)

    :param deps: ``(a, b)`` means `b` depends on `a`
    :raises ValueError: If there is no valid build order.
    """
    # Postorder DFS on dependency graph (b -> a means b depends on a) with cycle detection
    order = []      # type: List[T]
    seen = set()    # type: Set[T]
    graph = Graph[T](((b, a) for a, b in deps), projects)

    def visit(proj: T) -> bool:
        if proj in order:
            return True     # Already built
        if proj in seen:
            return False    # Cycle
        seen.add(proj)
        if not all(visit(child) for child in graph.neighbors(proj)):
            return False
        order.append(proj)  # All good, dependencies in order
        return True

    for project in projects:
        seen = set()
        if not visit(project):
            raise ValueError('No valid project order.')
    return order


def _is_valid_build_order(projects: Sequence[T], deps: Sequence[Tuple[T, T]], order: Sequence[T]) -> bool:
    depmap = {p: set() for p in projects}       # type: Dict[T, Set[T]]
    for dep, proj in deps:
        depmap[proj].add(dep)
    seen = set()        # type: Set[T]
    for proj in order:
        if not depmap[proj].issubset(seen):
            return False
        seen.add(proj)
    return True


def first_common_ancestor(tree: Node[T], p: Node[T], q: Node[T]) -> Node[T]:
    """:Return: the first common ancestor of nodes `p` and `q` in `tree`.  (4.8)

    Note identity is determined by the type T, and it must be hashable.
    """
    def find(nodes: Set[T], tree: Optional[Node[T]]) -> Union[Node[T], Set[T]]:
        """:Return: a subset of `nodes` found in `tree`, or the single node common ancestor of `nodes` if it exists."""
        if tree is None:
            return set()
        if nodes == {tree.data}:
            return tree
        lc = find(nodes, tree.left)
        if isinstance(lc, Node):
            return lc
        rc = find(nodes, tree.right)
        if isinstance(rc, Node):
            return rc
        found = lc | rc | ({tree.data} & nodes)
        if found == nodes:        # Found everything we're looking for
            return tree
        return found

    found = find({p.data, q.data}, tree)
    if isinstance(found, Node):
        return found
    raise ValueError('No common ancestor')


def _sorted_values(links: Iterable[Tuple[Optional[int], Optional[int], Optional[int]]]) -> List[int]:
    """:Return: the ordered set of unique values from an iterable of (parent, left, right) triples."""
    return sorted(frozenset((v for v in (chain.from_iterable(links)) if v is not None)))        # filter() does not typecheck as of mypy 0.511


def inorder(tree: Optional[Node[T]]) -> Iterable[Node[T]]:
    """:Return: the in-order traversal of nodes in `tree`."""
    if not tree:
        return ()
    return chain(inorder(tree.left), (tree,), inorder(tree.right))


class TestChapter4(unittest.TestCase):
    size = 100
    trees = {       # Map (parent, left, right) to height.  These are specific to the implementation, there are other shapes that meet the spec.
        ((0, None, None),): 1,
        ((1, 0, None),): 2,
        ((1, 0, 2),): 2,
        ((2, 1, 3), (1, 0, None)): 3,
        ((2, 1, 4), (1, 0, None), (4, 3, None)): 3,
        ((3, 1, 5), (1, 0, 2), (5, 4, None)): 3,
        ((3, 1, 5), (1, 0, 2), (5, 4, 6)): 3,
        ((4, 2, 6), (2, 1, 3), (1, 0, None), (6, 5, 7)): 4,
    }       # type: Dict[Tuple[Tuple[Optional[int], Optional[int], Optional[int]], ...], int]

    @staticmethod
    def random_sorted_array(n: int, mindiff: int = 1, maxdiff: int = 10) -> List[int]:
        """:Return: a sorted array of length `n` with (first) differences at least `mindiff` and at most `maxdiff`."""
        assert 0 <= mindiff <= maxdiff
        a = [random.randint(mindiff, maxdiff) for _ in range(n)]
        for i in range(1, len(a)):
            a[i] += a[i - 1]
        return a

    def test_height(self) -> None:
        for links, heit in self.trees.items():
            tree = _make_tree(links)
            self.assertEqual(height(tree), heit)

    def test_inorder(self) -> None:
        for links in self.trees:
            expected = _sorted_values(links)
            actual = list(map(lambda n: n.data, inorder(_make_tree(links))))
            self.assertListEqual(expected, actual)

    def test_minimal_tree(self) -> None:
        for links in self.trees:
            tree = _make_tree(links)
            values = _sorted_values(links)
            self.assertEqual(tree, minimal_tree(values))

        for _ in range(self.size):
            rvalues = self.random_sorted_array(random.randrange(1, self.size))
            rtree = minimal_tree(rvalues)
            self.assertEqual(height(rtree), int(log(len(rvalues), 2)) + 1)

    def test_list_of_depths(self) -> None:
        trees = {       # Map (parent, left, right) to list of values at each depth.
            ((0, None, None),): [[0]],
            ((1, 0, None),): [[1], [0]],
            ((1, 0, 2),): [[1], [0, 2]],
            ((2, 1, 3), (1, 0, None)): [[2], [1, 3], [0]],
            ((2, 1, 4), (1, 0, None), (4, 3, None)): [[2], [1, 4], [0, 3]],
            ((3, 1, 5), (1, 0, 2), (5, 4, None)): [[3], [1, 5], [0, 2, 4]],
            ((3, 1, 5), (1, 0, 2), (5, 4, 6)): [[3], [1, 5], [0, 2, 4, 6]],
            ((4, 2, 6), (2, 1, 3), (1, 0, None), (6, 5, 7)): [[4], [2, 6], [1, 3, 5, 7], [0]],
        }       # type: Dict[Tuple[Tuple[Optional[int], Optional[int], Optional[int]], ...], List[List[int]]]
        for links, levels in trees.items():
            tree = _make_tree(links)
            self.assertListEqual(list_of_depths(tree), levels)

    def test_check_balanced(self) -> None:
        for links in self.trees:
            self.assertTrue(check_balanced(_make_tree(links)))
        unbalanced = (
            ((0, None, 1), (1, None, 2), (2, None, 3), ),
            ((0, -1, 1), (1, None, 2), (2, None, 3), (3, None, 4),),
            ((0, -1, 1), (1, None, 2), (2, None, 3), (3, None, 4),),
            ((0, None, 1), (1, None, 2), (2, -2, 3), (3, None, 4),),
            ((0, -1, 1), (1, None, 2), (2, None, 3), (3, None, 4), (-1, -2, None), (-2, -3, None), (-3, -4, None)),
        )   # type: Tuple[Tuple[Tuple[Optional[int], Optional[int], Optional[int]], ...], ...]
        for ulinks in unbalanced:
            self.assertFalse(check_balanced(_make_tree(ulinks)))

    def test_validate_bst(self) -> None:
        for links in self.trees:
            self.assertTrue(validate_bst(_make_tree(links)))
        unsearch = (
            ((1, None, 0),),
            ((1, 2, None),),
            ((1, 0, 2), (0, 3, None)),
            ((1, 0, 2), (2, -1, None)),
            ((3, 1, 5), (1, 0, 2), (5, 4, 6), (0, 10, None)),
            ((3, 1, 5), (1, 0, 2), (5, 4, 6), (6, None, -1)),
            ((4, 2, 6), (2, 1, 3), (1, 10, None), (6, 5, 7)),
        )   # type: Tuple[Tuple[Tuple[Optional[int], Optional[int], Optional[int]], ...], ...]
        for ulinks in unsearch:
            self.assertFalse(validate_bst(_make_tree(ulinks)))

    def test_successor(self) -> None:
        for links in self.trees:
            tree = _make_tree(links)
            _add_parents(tree)
            nodes = tuple(inorder(tree))
            for i in range(len(nodes) - 1):
                self.assertEqual(successor(nodes[i]), nodes[i + 1])
            self.assertIsNone(successor(nodes[-1]))

    def test_build_order(self) -> None:
        buildable = (
            ((), ()),
            ((1,), ()),
            ((1, 2), ((2, 1),)),
            ((1, 2, 3), ((3, 2), (2, 1))),
            (('a', 'b', 'c', 'd', 'e', 'f'), (('a', 'd'), ('f', 'b'), ('b', 'd'), ('f', 'a'), ('d', 'c'))),
        )       # type: Tuple[Tuple[Tuple[Any, ...], Tuple[Tuple[Any, Any], ...]], ...]
        notsomuch = (
            ((1, 2), ((2, 1), (1, 2))),
            ((1, 2, 3), ((3, 2), (2, 1), (1, 3))),
        )   # type: Tuple[Tuple[Tuple[Any, ...], Tuple[Tuple[Any, Any], ...]], ...]

        for projects, deps in buildable:
            self.assertTrue(_is_valid_build_order(projects, deps, build_order(projects, deps)))
        for projects, deps in notsomuch:
            self.assertRaises(ValueError, build_order, projects, deps)

    @staticmethod
    def _first_common_ancestor(tree: Node[T], p: Node[T], q: Node[T]) -> Node[T]:
        """Requires parent pointers, defines identity by address (id(p), id(q))."""
        assert all(child is None or child.parent == tree for child in tree.children)
        p_ancestors, q_ancestors = set(), set()       # type: Set[int], Set[int]
        pans, qans = q, p   # type: Optional[Node[T]], Optional[Node[T]]
        while pans is not None or qans is not None:
            assert p_ancestors.isdisjoint(q_ancestors)

            if pans is not None:
                p_ancestors.add(id(pans))
                if not p_ancestors.isdisjoint(q_ancestors):
                    return pans
                pans = pans.parent

            if qans is not None:
                q_ancestors.add(id(qans))
                if not q_ancestors.isdisjoint(p_ancestors):
                    return qans
                qans = qans.parent

        raise ValueError('No common ancestor.')

    def test_first_common_ancestor(self) -> None:
        for links in self.trees:
            tree = _make_tree(links)
            _add_parents(tree)
            nodes = tuple(inorder(tree))
            for p, q in product(nodes, repeat=2):
                self.assertEqual(first_common_ancestor(tree, p, q), self._first_common_ancestor(tree, p, q))
            self.assertRaises(ValueError, first_common_ancestor, tree, Node(-1), Node(0))
