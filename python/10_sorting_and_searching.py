"""
Solutions to "Cracking The Coding Interview" problems on Sorting and Searching.
"""
import unittest
from itertools import combinations_with_replacement
import random


def search_in_rotated_array(a, x):
    """:Return: the index in sorted-but-rotated array `a` of item `x`, or None if not present. (10.3)"""
    start = find_start(a)
    if x <= a[-1]:
        return binary_search(a, x, start, len(a) - 1)
    else:
        return binary_search(a, x, 0, start - 1)


def find_start(a, lo=None, hi=None):
    """:Return: The (first) index of the smallest element in sorted-but-rotated array `a`, in log(len(a)) time."""
    lo = lo or 0
    hi = hi if hi is not None else len(a) - 1
    if lo >= hi:
        return lo
    mid = (lo + hi) // 2
    if a[mid - 1] > a[mid]:
        return mid
    if a[mid] > a[lo]:        # left normally ordered; either it's in the right, or it's lo
        s = find_start(a, mid, hi)
        return s if a[s - 1] > a[s] else lo
    elif a[mid] < a[lo]:    # right normally ordered; either it's in the left, or it's mid
        return find_start(a, lo, mid)
    else:       # a[mid] == a[lo]; left or right half is all identical
        if a[mid] != a[hi]:     # left identical; either it's in right, or it's lo
            s = find_start(a, mid + 1, hi)
            return s if a[s - 1] > a[s] else lo
        else:       # lo, mid, hi all identical; have to search both halves (degenerates to linear time)
            s = find_start(a, mid + 1, hi)
            return s if a[s - 1] > a[s] else find_start(a, lo, mid - 1)


def binary_search(a, x, lo=0, hi=None):
    """:Return: the index of `x` in sorted array `a`, or None if not present."""
    hi = hi if hi is not None else len(a) - 1
    while lo <= hi:
        mid = (lo + hi) // 2
        if x < a[mid]:
            hi = mid - 1
        elif x > a[mid]:
            lo = mid + 1
        else:
            return mid
    return None


class TestChapter10(unittest.TestCase):
    @staticmethod
    def all_sorted_arrays(n):
        """:Return: an iterator of all sorted arrays of [0..k-1] for k in [0, n]."""
        return (a for size in range(0, n + 1) for a in combinations_with_replacement(range(size), size))

    @staticmethod
    def random_sorted_array(n, maxdiff=10):
        """:Return: a sorted array of length `n` with (first) differences <= `maxdiff`."""
        a = [random.randint(0, maxdiff) for _ in range(n)]
        for i in range(1, len(a)):
            a[i] += a[i - 1]
        return a

    @staticmethod
    def rotate(a, n):
        """:Return: a copy of array `a` rotated right by `n` elements."""
        n %= len(a)
        return a[n:] + a[:n]

    def test_binary_search(self):
        for a in self.all_sorted_arrays(10):
            for x in a:
                self.assertEqual(a[binary_search(a, x)], x)
            self.assertIsNone(binary_search(a, -1))
            self.assertIsNone(binary_search(a, len(a)))

        for _ in range(500):
            a = self.random_sorted_array(random.randint(1, 100))
            for x in a:
                self.assertEqual(a[binary_search(a, x)], x)
            self.assertIsNone(binary_search(a, -1))
            self.assertIsNone(binary_search(a, max(a) + 1))
            for x in frozenset(range(100)).difference(a):
                self.assertIsNone(binary_search(a, x))

    def test_find_start(self):
        for a in self.all_sorted_arrays(8):
            for r in range(len(a)):
                a = self.rotate(a, r)
                start = next((i for i in range(1, len(a)) if a[i] < a[i - 1]), 0)       # Better way?
                self.assertEqual(find_start(a), start, '{} {}'.format(r, a))

    def test_search_in_rotated_array(self):
        for a in self.all_sorted_arrays(7):
            for r in range(len(a)):
                a = self.rotate(a, r)
                for x in a:
                    self.assertEqual(a[search_in_rotated_array(a, x)], x)
                self.assertIsNone(search_in_rotated_array(a, -1))
                self.assertIsNone(search_in_rotated_array(a, len(a)))

        for _ in range(50):
            a = self.random_sorted_array(random.randint(1, 100))
            for r in range(len(a)):
                a = self.rotate(a, r)
                for x in a:
                    self.assertEqual(a[search_in_rotated_array(a, x)], x)
                self.assertIsNone(search_in_rotated_array(a, -1))
                self.assertIsNone(search_in_rotated_array(a, max(a) + 1))
                for x in frozenset(range(100)).difference(a):
                    self.assertIsNone(search_in_rotated_array(a, x))
