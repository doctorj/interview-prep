Interview Preparation Code
==========================

Things to review before you interview for a programming job.

Based on questions from "Cracking The Coding Interivew", 6th edition, by Gayle Laakmann McDowell.
Solutions in Python 3.4 (with type annotations).

Data Structures
***************

`Stack <https://gitlab.com/doctorj/interview-prep/tree/master/python/stack.py>`_
--------------------------------------------------------------------------------
First-in, last-out.  Basic array-based implementation with random access (why does anybody do this with a linked list?).

`Queue <https://gitlab.com/doctorj/interview-prep/tree/master/python/queue.py>`_
--------------------------------------------------------------------------------
First-in, first-out.  Basic array-based ring buffer implementation with random access.

`Linked List <https://gitlab.com/doctorj/interview-prep/tree/master/python/linkedlist.py>`_
-------------------------------------------------------------------------------------------
Basic singly-linked list implementing the `list` interface and part of the `deque` interface.

`Heap (Priority Queue) <https://gitlab.com/doctorj/interview-prep/tree/master/python/heap.py>`_
-----------------------------------------------------------------------------------------------
Complete binary tree with each node less than or equal to its children (for a min heap).
Stored in array, min at index 0.  ``push(item)`` puts new item at end of array, swaps with parent
(at index ``(k - 1) // 2``) while less than parent; O(log n).  ``pop()`` removes index 0, puts last item
at index 0, and swaps with child while smallest child less than item ("sift down"); O(log n).  ``heapify()`` moves from
the parent of the last element to element 0, assuming the children are valid heaps and doing a sift down on each;
O(n).

`Hash Table <https://gitlab.com/doctorj/interview-prep/tree/master/python/hashtable.py>`_
-----------------------------------------------------------------------------------------
Basic separate-chaining (with dynamic arrays!) implementation.

`Trie (Prefix Tree) <https://gitlab.com/doctorj/interview-prep/tree/master/python/trie.py>`_
--------------------------------------------------------------------------------------------
N-ary tree with one element of a sequence (often a character) stored at each node, and special terminal nodes
or flags to mark the end of a sequence (often a word; needed because a node can be both the end of one word and a prefix of another).
Lookups are O(k) for a k-length sequence.

`Graph <https://gitlab.com/doctorj/interview-prep/tree/master/python/graph.py>`_
--------------------------------------------------------------------------------
Adjacency-list based implementation with support for undirected edges and dynamic node/edge removal.


Problems and Solutions
**********************

1. `Arrays and Strings <https://gitlab.com/doctorj/interview-prep/tree/master/python/01_arrays_and_strings.py>`_
2. `Linked Lists <https://gitlab.com/doctorj/interview-prep/tree/master/python/02_linked_lists.py>`_
3. `Stacks and Queues <https://gitlab.com/doctorj/interview-prep/tree/master/python/03_stacks_and_queues.py>`_
4. `Trees and Graphs <https://gitlab.com/doctorj/interview-prep/tree/master/python/04_trees_and_graphs.py>`_
5. `Bit Manipulation <https://gitlab.com/doctorj/interview-prep/tree/master/python/05_bit_manipulation.py>`_
6. `Math and Logic Puzzles <https://gitlab.com/doctorj/interview-prep/tree/master/python/06_math_and_logic_puzzles.py>`_
7. Object Oreinted Design
8. `Recursion and Dynamic Programming <https://gitlab.com/doctorj/interview-prep/tree/master/python/08_recursion_and_dynamic_programming.py>`_
9. System Design and Scalability
10. `Sorting and Searching <https://gitlab.com/doctorj/interview-prep/tree/master/python/10_sorting_and_searching.py>`_
11. Testing
12. C and C++
13. Java
14. Databases
15. Threads and Locks
16. Moderate
17. Hard


Sorting Algorithms
******************
Bubble Sort (O(n^2) time, O(1) space, stable):
    "Bubble" largest element to end by comparing adjacent pairs, swap if out of order.
    Pass over entire array each time until no swaps are needed.

Selection Sort (O(n^2) time, O(1) space, stable):
    Select smallest element and put it first; find 2nd smallest and put it 2nd; repeat n times.

Insertion Sort (O(n^2) time, O(1) space, stable):
    Assume 0..i - 1 sorted, take i and insert it where it goes, by swapping with i-1, i-2, ... until no need to swap.

Merge Sort (O(nlogn) time, O(n) space, stable):
    Recursively split in half until size 1, then merge sorted halves.
    To merge copy halves into aux arrays, then merge back into original array.

Quick Sort (O(n^2) time, O(logn) space, not stable):
    Pick random partition element, put everything <= partition before it, everything > after.
    Recurse on both partitions.  O(nlogn) on average; worst case happens when pivot is smallest or largest
    (including all identical elements).

Heap Sort (O(nlogn) time, O(1) space, stable):
    Build a max heap of the array (max at index 0). While heap length > 1,
    swap first element (largest) with last, decrease heap size by 1 (exclude last element).
    Sift down new first element.

Radix Sort (O(kn) time (k digits), O(k) space, stable):
    For digit d from least significant to most, use a stable sort to sort entire array by digit d.
    With counting sort, each of k passes is O(n), so O(k*n).
